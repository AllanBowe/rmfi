/* location for output files.  Make this an empty location as there are LOTS of files! */
%let out_dir=C:\TEMP\PNX\;

/* location of source code */
%let sourcecode=Y:\Documents\rmfi\CodeWeb.sas;
%let root=C:\Program Files\SAS\SASFoundation\9.2;

/* a number of sas scripts contain large volumes of DATALINES */
/* this switch will identify those scripts (by filesize) and remove the contents */
/* (but leave the code) */
%let filesize_switch=1; 
%let timestamp=%sysfunc(datetime(),datetime19.);

options nomlogic nomprint;
options noquotelenmax;
options nonotes;
options nomprint;
data _1dirlist;
infile cards dsd dlm=',' missover;
input path :$500.;
if substr(reverse(trim(path)),1,1) not in ("\","/") then path=cats(path,"\"); 
path=cats("&root.",path);
call symput('PATH'||left(_N_),trim(path));
call symput('num_of_dirs',_n_);
cards; 
\rmicomnsvr
\rmicomnsvr\sasmacro
\rmicomnsvr\sasmisc\batch
\rmicomnsvr\sasmisc\martddl
\rmicomnsvr\sasmisc\martddl\mapping
\rmicomnsvr\sasmisc\martddl\mart
\rmicomnsvr\sasmisc\martddl\staging
\rmicomnsvr\sasmisc\martddl\static
\rmicomnsvr\sasmisc\reportmart
\rmicomnsvr\sasmisc\solution_data_mart
\rmicomnsvr\sasmisc\solution_data_mart\env_rsk
\rmicomnsvr\sasmisc\solution_data_mart\env_rsk\data
\rmicomnsvr\sasmisc\solution_data_mart\env_rsk\fea
\rmicomnsvr\sasmisc\solution_data_mart\env_rsk\fea\data
\rmicomnsvr\sasmisc\solution_data_mart\env_rsk\fea\functions
\rmicomnsvr\sasmisc\solution_data_mart\env_rsk\fea\methods
\rmicomnsvr\sasmisc\solution_data_mart\env_rsk\fincad
\rmicomnsvr\sasmisc\solution_data_mart\env_rsk\fincad\data
\rmicomnsvr\sasmisc\solution_data_mart\env_rsk\fincad\functions
\rmicomnsvr\sasmisc\solution_data_mart\env_rsk\fincad\methods
\rmicomnsvr\sasmisc\solution_data_mart\env_rsk\functions
\rmicomnsvr\sasmisc\solution_data_mart\env_rsk\methods
\rmicomnsvr\sasmisc\solution_data_mart\multi_app\methods
\rmicomnsvr\sasmisc\solution_data_mart\multi_app\functions
\rmicomnsvr\sasmisc\solution_data_mart\rmi_env\data
\rmicomnsvr\sasmisc\solution_data_mart\rmi_env
\rmicomnsvr\sasmisc\solution_data_mart\sampledata
\rmicomnsvr\sasmisc\solution_data_mart\sampledata\mapping
\rmicomnsvr\sasmisc\solution_data_mart\sampledata\mart
\rmicomnsvr\sasmisc\solution_data_mart\sampledata\staging
\rmicomnsvr\sasmisc\solution_data_mart\sampledata\static
\rmicomnsvr\sasmisc\system_data
\rmicomnsvr\sasstp
\rmicomnsvr\ucmacros
\rmifirmmva\ucmacros
\rmifirmmva\sasmisc\solution_data_mart\fw_env
\rmifirmmva\sasmisc\solution_data_mart\fw_env\methods
\rmifirmmva\sasstp
\rmifirmmva\sasmisc\batch
\rmilifemva\ucmacros
\rmilifemva\sasmisc\batch
\rmilifemva\sasmisc\solution_data_mart\life_env
\rmilifemva\sasmisc\solution_data_mart\life_env\data
\rmilifemva\sasmisc\solution_data_mart\life_env\functions
\rmilifemva\sasmisc\solution_data_mart\life_env\methods
\rmilifemva\sasstp
\rmimktmva\ucmacros
\rmimktmva\sasmisc\batch
\rmimktmva\sasmisc\solution_data_mart\mkrsk_env
\rmimktmva\sasmisc\solution_data_mart\mkrsk_env\data
\rmimktmva\sasmisc\solution_data_mart\mkrsk_env\functions
\rmimktmva\sasmisc\solution_data_mart\mkrsk_env\methods
\rmimktmva\sasstp
\rmipcmva\ucmacros
\rmipcmva\sasmisc\batch
\rmipcmva\sasmisc\solution_data_mart\nl_env
\rmipcmva\sasmisc\solution_data_mart\nl_env\data
\rmipcmva\sasmisc\solution_data_mart\nl_env\functions
\rmipcmva\sasmisc\solution_data_mart\nl_env\methods
\rmipcmva\sasstp
\rskrptmrtvrt\ucmacros
\rskrptmrtvrt\sasmisc\ddls
\rskrptmrtvrt\sasmisc\programs
;
run;
/* linux equivalents */
/*
/opt/sas/env2/lev1/software/SASFoundation/9.2/ucmacros/rmicomnsvr/
/opt/sas/env2/lev1/software/SASFoundation/9.2/ucmacros/rmifirmmva/
/opt/sas/env2/lev1/software/SASFoundation/9.2/ucmacros/rmilifemva/
/opt/sas/env2/lev1/software/SASFoundation/9.2/ucmacros/rmimktmva/
/opt/sas/env2/lev1/software/SASFoundation/9.2/ucmacros/rmipcmva/
/opt/sas/env2/lev1/software/SASFoundation/9.2/ucmacros/rskrptmrtvrt/
/opt/sas/env2/lev1/software/SASFoundation/9.2/sasstp/rmicomnsvr/
/opt/sas/env2/lev1/software/SASFoundation/9.2/sasstp/rmifirmmva/
/opt/sas/env2/lev1/software/SASFoundation/9.2/sasstp/rmilifemva/
/opt/sas/env2/lev1/software/SASFoundation/9.2/sasstp/rmimktmva/
/opt/sas/env2/lev1/software/SASFoundation/9.2/sasstp/rmipcmva/
/opt/sas/env2/lev1/software/SASFoundation/9.2/sasautos/
/opt/sas/env2/lev1/software/SASFoundation/9.2/misc/rskrptmrtvrt/ddls/
/opt/sas/env2/lev1/software/SASFoundation/9.2/misc/rskrptmrtvrt/programs/
/opt/sas/env2/lev1/software/SASFoundation/9.2/misc/rmicomnsvr/solution_data_mart/sampledata/static
*/

data _2dslist (keep=buffer macroname) ;
set _1dirlist;
if filename('x',path) = 0 then do;
	did=dopen('x');
	if (did = 0) then do;
		msg= 'ERROR: '||trim(path)||' is NOT A DIR.  Sysmsg='||sysmsg();
		put msg=;
		goto exitloop;
	end;
	num_members = dnum(did);
	put num_members=;
	do cnt=1 to num_members;
		buffer=trim(path)||dread(did,cnt);
		macroname=lowcase(scan(buffer,-2,'/\.')); /* get macro name */
		if upcase(scan(buffer,-1)) in ('SAS','SMD') then continue; 
/*		if index(buffer,'sasautos') and not (macroname=:'r') then continue;*/
		output;
	end;

	exitloop:;
	rc=dclose(did);
end;
run;

proc sort data=_2dslist out=_3dslistsort;
by macroname;
run;


data _4dirlist;
file "&out_dir.dups.htm";
set _3dslistsort end=lastobs;
by macroname;
/* work out file size of each macro.  WILL ONLY WORK IN 9.2 - but is OS independent */
length filesize $60.;
drop rc fid close;
rc=filename("myfile",buffer);
fid=fopen("myfile");
filesize=finfo(fid,"File Size (bytes)");
close=fclose(fid);
/* info is used to subsequently reduce the file sizes by removing datalines */
if filesize >1000000 and &filesize_switch.=1 then do;
	z+1; /* iterator */
	call symput('Reduce'||left(z),cats("&out_dir",scan(macroname,1),".htm"));
	call symput('ReduceCNT',z);
end;

/* load mac paths into memory */
call symput('MacPath'||left(_n_), trim(buffer));
call symput('MacName'||left(_n_), trim(macroname));

if _n_=1 then put "<h1> List of duplicates </h1>"
	"<p> These may or may not be actually called within RMfI as the analysis"
	" has not been done against SASAUTOS. </p>"
	"<table border=1><tr><th>Macro</th><th>Path</th></tr>";

if first.macroname then dup=0;

if first.macroname ne 1 or last.macroname ne 1 then do;
	DUP+1;
	put '<tr><td>';
	if dup>1 then dupname=cats(macroname,'_dup',dup);
	else dupname=macroname;
	line="<a name='"||trim(macroname)||
		"' href= '"||
		cats(dupname,".htm'>",macroname,"</a></td><td>",buffer);
	put line;
	put "</td></tr>";
end;


if lastobs then call symput('NOBS',_n_);
run;

/* create table to contain the tree */
proc sql;
create table _5tree ( parent CHAR(50), child CHAR(50) );



%macro x;
/* loop through each macro in the dirlist table, converting them 
	into htm pages, swapping each RMI macro call for a hyperlink 
	to the relevant macro.  This is done in a single datastep to 
	avoid loading the dirlist table into memory every time */
%do x=1 %to &nobs.;
	data newmacs (keep=parent child);
	format parse parse1 parse2 infile macroname outref buffer $300.;
	format parent child $50.;
	call missing(dup);
	if _n_=1 then do;
		/* load list of drillable macros */
		dcl hash mac(dataset:'_4dirlist(keep=macroname)');
		rc = mac.defineKey('macroname');
		rc = mac.defineDone( );

		/* determine duplicate based on absolute path */
		dcl hash dupLK(dataset:'_4dirlist(keep=buffer dup)');
		rc = dupLK.definekey('buffer');
		rc = dupLK.definedata('DUP');
		rc = dupLK.defineDone( );

		macroname="&&macname&x";
		if mac.find() ne 0 then do;
			put 'SERIOUS ISSUE WITH MACRO LOOKUP!!';
			abort;
		end;

		buffer="&&macpath&x";
		
		if dupLK.find()=0 then  
			if dup>1  then outref= cats("&out_dir.&&macname&x.._dup",dup,".htm");
		else outref="&out_dir.&&macname&x...htm";

		retain parent;
		parent="&&macname&x";
	end;
	file dummyref filevar=outref;

	infile "&&macpath&x" truncover end=eof;

	put "<head><style>pre {margin-top:0;margin-bottom:0;}"
		"</style></head><body><pre>/*<b> " 
		"&&macpath&x (generated on &timestamp)" @;
	if DUP>0 then put /"<h1> THIS MACRO HAS <a href=dups.htm#%left(&&macname&x)> DUPLICATES!!</a></h1>";
	put "<h6> Click for <a href=&&macname&x...html> Hierarchy Tree </a> or <a href=_all_macros.htm> _all_macros </a></h6>";
	put "</b> */</pre><pre>";
	do until (eof=1);
		input  ;
		/* several macro calls are based on PRODUCT (eg %rmi_ or %rmb_)     */
		/* so lets resolve mac refs (%&product._) to allow macro drill thru */
		parse1=prxchange('s/%&product\._/%rmi_/i',-1,_infile_);
		infile=parse1;
		i=1;
		/* loop through any macros that exist in the line */
		/*(may break if same macro called twice in same line)*/
		if index(_infile_,'%') ne 0 then do until (macroname=''); 
			macroname=lowcase(scan(infile,i,'%;() /.='));
			if mac.find()=0 then do;/* pick up %macname calls case insensitively */
				parse1=	prxchange(
					cats('s/%',macroname,'/<a href=',macroname,'.htm>%',macroname,'<\/a>/i')
					,-1,parse1);
				parse1=	prxchange(  /* pick up macname.sas (SP) refs */
					cats('s/',macroname,'.sas/<a href=',macroname,'.htm>',macroname,'.sas<\/a>/i')
					,-1,parse1);
				parse1=	prxchange(  /* pick up "macname(" (risk dimension function) refs */
					cats('s/',macroname,'\(/<a href=',macroname,'.htm>',macroname,'(<\/a>/i')
					,-1,parse1);
				child=macroname;
				output;
			end;
			i+1;
		end;
		/* use hidden char to retain trailing blanks */
		if substr(parse1,1,1)=' ' then parse2='00'x||trim(parse1); 
		else parse2=trim(parse1);
		/* convert ampersands to avoid final html confusing escape characters */
		parse=tranwrd(parse2,'&','&amp;');
		put  parse: $char. ;
		if eof then do;
			put '</pre></body>';
			stop;
		end;
	end;
	run;
	proc append base=_5tree data=newmacs;run;
%end;
%mend; %x

proc sort data=_5tree out=_6uniquetree nodup;
by parent child;
run;

%macro apply_tree(macname);
proc sql;
create table parents as
	select distinct parent from _6uniquetree
	where child="&macname" and parent ne "&macname";
create table children as
	select distinct child from _6uniquetree
	where parent="&macname" and child ne "&macname";

data _null_;
file "&out_dir.&macname..html"; /* html - not htm! */
format str $1000.;
if 0 then set parents nobs=pnobs;
if 0 then set children nobs=cnobs;
totnobs=max(pnobs,cnobs);
if _n_=1 then put "<html><p><a href=_all_macros.htm> _all_macros </a></p><h1>&macname - Macro Tree</h1>"
	"<p> Generated on &timestamp. </p>"
	"<table border=1><tr><th>Parents</th><th>Macro</th><th>Children</th></tr>";
do i=1 to totnobs;
	if i <= pnobs then set parents;
	if i <= cnobs then set children;

	if i=1 and pnobs=0 then put "<tr><td>(None)</td>";
	else if i> pnobs then put "<tr><td></td>";
	else do;
		str=cats("<tr><td><a href='",parent,".html'>",parent,"</a>(<a href='"
			,parent,".htm'>open</a>)</td>");
		put str ;
	end;
		
	if i=1 then put "<td>&macname (<a href='&macname..htm'>open</a>)</td>";
	else put "<td></td>";

	if i=1 and cnobs=0 then put "<td>(None)</td></tr>";
	else if i>cnobs then put "<td></td></tr>";
	else do;
		str=cats("<td><a href='",child,".html'>",child,"</a>(<a href='"
			,child,".htm'>open</a>)</td></tr>");
		put str ;
	end;
end;
put "</table><p><a href=_all_macros.htm> _all_macros </a></p></html>";
run;

%mend;

/*%apply_tree(X_rmif_b20_proc_risk_margin_fra);*/


proc sort data=_4dirlist(keep=macroname) out=_7allmacs nodupkey;  by macroname;run;
proc sql undo_policy=none; /* sorted twice as using distinct below causes warning */
create table _7allmacs as 
select macroname from _7allmacs
order by lowcase(macroname);

data _null_;
format str $500.;
set _7allmacs end=lastobs;
file "&out_dir._all_macros.htm";
if _n_=1 then put "<html><h1>RMFI Macros</h1>"
	"<p> This is a unique list of RMFI macros (and pricing methods), contained in "
	"<a href=_source_folders.htm>these directories</a>, "
	" generated on &timestamp.." 
	"Click <a href=_source_code.txt>here</a> for source code.</p><ol>";
	
str=cats("<li><a href='",macroname,".html'>",macroname,"</a> (<a href='"
	,macroname,".htm'>open</a>)</li>");
put str;
if lastobs then put "</ol></html>";
call execute(cats('%apply_tree(',macroname,');'));
run;

data _null_;
file "&out_dir._source_code.txt";
infile "&sourcecode" ;
input;
put _infile_;
run;

data _null_;
set _1dirlist end=lastobs;
file "&out_dir._source_folders.htm";
if _n_=1 then put "<html><h1>RMFI Source Code directories</h1>"
	"<p> This is the list of directories searched for .sas files "
	" as used in this list of <a href=_all_macros.htm> RMFI Macros </a>"
	" (generated on &timestamp)</p><ul>";
put "<li> " path "</li>";
if lastobs then put "</ul></html>";
run;

%macro y;
%if %symexist(reducecnt)=0 %then %let reducecnt=0;
%do x=1 %to &ReduceCNT.;
	data _null_;
	infile "&&reduce&x";
	file "&&reduce&x..TEMP";
	if _n_=1 then put 
		'<h1> ANY DATALINES HAVE BEEN REMOVED DUE TO FILE SIZE ISSUES!!!! See below </h1>';
	input;
	if _infile_=:'cards' then do;
		do x=1 to 10;
			input; put _infile_;
		end;
		put '</pre></br></br><b> These DATALINES removed. </b></br></br><pre>';
		do until (_infile_=:';;');
			input;
		end;
		put _infile_;
	end;
	else put _infile_;
	run;
	/* now delete the old file and rename the new */
	data _null_;
	fname="ref";
	rc=filename(fname,"&&reduce&x");
	if rc=0 then rc=fdelete(fname);
	rc=filename(fname);
	rc=rename("&&reduce&x..TEMP","&&reduce&x","file");
	run;

%end;
%mend; %y;


