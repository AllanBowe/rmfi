/********************************************************************** 
 * Copyright (c) 2010 by SAS Institute Inc., Cary, NC, USA.             
 *                                                                      
 * NAME:          rmi_post_init                                         
 *                                                                      
 * PURPOSE:       Called at the end of %rmiinit after the SASAUTOS      
 *                macro path has been set up. Performs any other        
 *                customer-specific initialization  during startup.     
 *                This may include additional macro paths, libname      
 *                assignments, or global macro variable definitions.    
 *                                                                      
 * USAGE:         %rmi_post_init(VERSION=)                              
 *                                                                      
 * PARAMETERS:    VERSION     The product version number                
 *                            that was passed to %rmiinit.             
 *                                                                      
 **********************************************************************/
%macro rmi_post_init(VERSION=);

   /*--------------------------------------------------------------------------
    * If any additional autocall macro directories are required  add them here.
    * Note that paths must be quoted.    
    *--------------------------------------------------------------------------*/

/* this script assumes that the phoenix codebase was installed in the SOURCE directory.  
   If this is not the case, then simply update the below to point at phoenix directly.. */

options sasautos=(&ORIGINAL_SASAUTOS);

options insert=(SASAUTOS="&RSK_DEPLOYMENT_ROOT/CVS/riskrpt/ucmacros/en");

options insert=(SASAUTOS="&RSK_DEPLOYMENT_ROOT/CVS/rmicmn/misc/batch");

options insert=(SASAUTOS="&RSK_DEPLOYMENT_ROOT/CVS/rmicmn/ucmacros/en");

options insert=(SASAUTOS="&RSK_DEPLOYMENT_ROOT/CVS/rmifra/misc/batch");

options insert=(SASAUTOS="&RSK_DEPLOYMENT_ROOT/CVS/rmifra/ucmacros/en");

options insert=(SASAUTOS="&RSK_DEPLOYMENT_ROOT/CVS/rmimkrsk/misc/batch");

options insert=(SASAUTOS="&RSK_DEPLOYMENT_ROOT/CVS/rmimkrsk/ucmacros/en");

options insert=(SASAUTOS="&RSK_DEPLOYMENT_ROOT/CVS/rmsrvcmn/ucmacros/en");

options insert=(SASAUTOS="&RMX_CUSTOM_PATH/rmi_macros_customised");  /* custom path */

options insert=(SASAUTOS="&RMX_CUSTOM_PATH/rmi_macros_customised_until_next_hf"); /* custom path */

%let rrrprogpath = &RSK_DEPLOYMENT_ROOT/CVS/riskrpt/misc/programs; 

%let rrrddlpath= &RSK_DEPLOYMENT_ROOT/CVS/riskrpt/misc/ddls;
 
%mend;
