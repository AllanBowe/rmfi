 
%macro makeSymlink(pathSymLink= ,path=, symLink=, OS=LAX, File=N);
     %if &OS = LAX %then %do;
           X cd &pathSymLink.;
           X ln -s "&path." &symLink.;
     %end;
     %else %if &OS = WIN %then %do;
 
           X cd &pathSymLink.;
           %if &File = Y %then %do; 
                X mklink "&symlink." "&path.";
           %end;
           %else %do; 
                X mklink /D &symlink. "&path.";
           %end;
     %end;
%mend;
%macro delSymLink(pathSymLink =, symLink=, OS=LAX, File=N);
     %if &OS = LAX %then %do;
 
           X cd &pathSymLink.;
           X rm &symLink.;
     %end;
     %else %if &OS = WIN %then %do;
X d:;
X cd &pathSymLink.;
%let search=.;
           %if &File = Y %then %do; 
                X del &symlink.;
           %end;
           %else %do; 
                X rmdir &symlink.;
           %end;
 
     %end;
%mend;
 
%macro createDir(path=, OS=LAX);
     %if &OS = LAX %then %do;
           X mkdir &path.;
     %end;
     %else %if &OS = WIN %then %do;
           X mkdir "&path.";
     %end;
%mend;
%macro delDir(path=, OS=LAX);
     %if &OS = LAX %then %do;
           X rm &path.;
     %end;
     %else %if &OS = WIN %then %do;
           X rmdir "&path.";
     %end;
%mend;
%macro newSymlink(pathSymLink= ,path=, symLink=, OS = LAX, File=N);
     %delSymLink(pathSymlink=&pathSymlink., symlink=&symlink., OS = &OS. File=&File);
     %makeSymlink(pathSymLink=&pathSymLink. ,path=&path., symLink=&symLink., OS = &OS., File=&File);
%mend;
 
%macro windowsToWindows (deployementRootWinSource=, deployementRootWinTarget=, CVS_Like=1);
options noxwait;
           %newSymlink(pathSymlink=&deployementRootWinSource.\rmicomnsvr\,   symlink= ucmacros,     path=&deployementRootWinTarget.\CVS\rmicmn\ucmacros\en\, OS = WIN);
           %newSymlink(pathSymlink=&deployementRootWinSource.\rmifirmmva\,   symlink= ucmacros,     path=&deployementRootWinTarget.\CVS\rmifra\ucmacros\en\, OS = WIN);
           %newSymlink(pathSymlink=&deployementRootWinSource.\rmilifemva\,   symlink= ucmacros,     path=&deployementRootWinTarget.\CVS\rmilife\ucmacros\en\, OS = WIN);
           %newSymlink(pathSymlink=&deployementRootWinSource.\rmipcmva\,   symlink= ucmacros,       path=&deployementRootWinTarget.\CVS\rmiprpcs\ucmacros\en\, OS = WIN);
           %newSymlink(pathSymlink=&deployementRootWinSource.\rmimktmva\,   symlink= ucmacros,      path=&deployementRootWinTarget.\CVS\rmimkrsk\ucmacros\en\, OS = WIN);
 
           %newSymlink(pathSymlink=&deployementRootWinSource.\rmicomnsvr\,     symlink= sasstp,     path=&deployementRootWinTarget.\CVS\rmicmn\stp\en\, OS = WIN);
           %newSymlink(pathSymlink=&deployementRootWinSource.\rmifirmmva\,     symlink=sasstp ,     path=&deployementRootWinTarget.\CVS\rmifra\stp\en\, OS = WIN);
           %newSymlink(pathSymlink=&deployementRootWinSource.\rmilifemva\,     symlink=sasstp ,     path=&deployementRootWinTarget.\CVS\rmilife\stp\en\, OS = WIN);
           %newSymlink(pathSymlink=&deployementRootWinSource.\rmipcmva\,     symlink=sasstp ,       path=&deployementRootWinTarget.\CVS\rmiprpcs\stp\en\, OS  = WIN);
           %newSymlink(pathSymlink=&deployementRootWinSource.\rmimktmva\,     symlink=sasstp ,      path=&deployementRootWinTarget.\CVS\rmimkrsk\stp\en\, OS = WIN);
 
           %delDir(path=&deployementRootWinSource.\rmicomnsvr\sasmisc\, OS = WIN);
           %createDir(path=&deployementRootWinSource.\rmicomnsvr\sasmacro\, OS = WIN);
           %createDir(path=&deployementRootWinSource.\rmicomnsvr\sasmisc\, OS = WIN);
           %createDir(path=&deployementRootWinSource.\rmicomnsvr\sasmisc\sashelp\, OS = WIN);
           %createDir(path=&deployementRootWinSource.\rmicomnsvr\sasmisc\solution_data_mart\, OS = WIN);
 
           %newSymlink(pathSymlink=&deployementRootWinSource.\rmicomnsvr\sasmisc\,       symlink=system_data , path=&deployementRootWinTarget.\CVS\rmicmn\misc\system_data\, OS = WIN);
           %newSymlink(pathSymlink=&deployementRootWinSource.\rmicomnsvr\sasmisc\,       symlink=reportmart ,path=&deployementRootWinTarget.\CVS\rmicmn\misc\reportmart\, OS = WIN);
           %newSymlink(pathSymlink=&deployementRootWinSource.\rmicomnsvr\sasmisc\,       symlink=rptimages , path=&deployementRootWinTarget.\CVS\rmicmn\misc\rptimages\, OS = WIN);
           %newSymlink(pathSymlink=&deployementRootWinSource.\rmicomnsvr\sasmisc\,       symlink=batch,      path=&deployementRootWinTarget.\CVS\rmicmn\misc\batch\, OS = WIN);
           %newSymlink(pathSymlink=&deployementRootWinSource.\rmicomnsvr\sasmisc\,       symlink=martddl ,   path=&deployementRootWinTarget.\CVS\rmicmn\misc\martddl\, OS = WIN);
           %newSymlink(pathSymlink=&deployementRootWinSource.\rmicomnsvr\sasmisc\,       symlink=Config ,    path=&deployementRootWinTarget.\CVS\rmicmn\misc\Config\, OS = WIN);
 
           %newSymlink(pathSymlink=&deployementRootWinSource.\rmicomnsvr\sasmisc\solution_data_mart\,       symlink=rd_env ,     path=&deployementRootWinTarget.\CVS\rmicmn\misc\solution_data_mart\rd_env\, OS = WIN);
           %newSymlink(pathSymlink=&deployementRootWinSource.\rmicomnsvr\sasmisc\solution_data_mart\,       symlink=rmi_env ,     path=&deployementRootWinTarget.\CVS\rmicmn\misc\solution_data_mart\rmi_env\, OS = WIN);
           %newSymlink(pathSymlink=&deployementRootWinSource.\rmicomnsvr\sasmisc\solution_data_mart\,       symlink=multi_app ,     path=&deployementRootWinTarget.\CVS\rmicmn\misc\solution_data_mart\multi_app\, OS = WIN);
           %newSymlink(pathSymlink=&deployementRootWinSource.\rmicomnsvr\sasmisc\solution_data_mart\,       symlink=sampledata ,     path=&deployementRootWinTarget.\CVS\rmicmn\misc\solution_data_mart\sampledata\, OS = WIN);
           %newSymlink(pathSymlink=&deployementRootWinSource.\rmicomnsvr\sasmisc\solution_data_mart\,       symlink=sampledata_append ,     path=&deployementRootWinTarget.\CVS\rmicmn\misc\solution_data_mart\sampledata_append\, OS = WIN);
           %newSymlink(pathSymlink=&deployementRootWinSource.\rmicomnsvr\sasmisc\solution_data_mart\,       symlink=env_rsk ,     path=&deployementRootWinTarget.\CVS\rmsrvcmn\misc\solution_data_mart\env_rsk, OS = WIN);
 
           %newSymlink(pathSymlink=&deployementRootWinSource.\rmicomnsvr\sasmisc\solution_data_mart\,       symlink=rmi_create_sample_data.sas ,     path=&deployementRootWinTarget.\CVS\rmicmn\misc\solution_data_mart\rmi_create_sample_data.sas, OS = WIN, File=Y);
           %newSymlink(pathSymlink=&deployementRootWinSource.\rmicomnsvr\sasmisc\solution_data_mart\,       symlink=rmi_create_solution_data_mart.sas ,     path=&deployementRootWinTarget.\CVS\rmicmn\misc\solution_data_mart\rmi_create_solution_data_mart.sas, OS = WIN, File=Y);
           %newSymlink(pathSymlink=&deployementRootWinSource.\rmicomnsvr\sasmisc\solution_data_mart\,       symlink=rmi_add_new_configuration_set.sas ,     path=&deployementRootWinTarget.\CVS\rmicmn\misc\solution_data_mart\rmi_add_new_configuration_set.sas, OS = WIN, File=Y);
           %newSymlink(pathSymlink=&deployementRootWinSource.\rmicomnsvr\sasmisc\solution_data_mart\,       symlink=rmi_initdata_mapping.sas ,     path=&deployementRootWinTarget.\CVS\rmicmn\misc\solution_data_mart\rmi_initdata_mapping.sas, OS = WIN, File=Y);
            %newSymlink(pathSymlink=&deployementRootWinSource.\rmicomnsvr\sasmisc\solution_data_mart\,       symlink=rmi_initdata_mart.sas ,     path=&deployementRootWinTarget.\CVS\rmicmn\misc\solution_data_mart\rmi_initdata_mart.sas, OS = WIN, File=Y);
           %newSymlink(pathSymlink=&deployementRootWinSource.\rmicomnsvr\sasmisc\solution_data_mart\,       symlink=rmi_initdata_staging.sas ,     path=&deployementRootWinTarget.\CVS\rmicmn\misc\solution_data_mart\rmi_initdata_staging.sas, OS = WIN, File=Y);
           %newSymlink(pathSymlink=&deployementRootWinSource.\rmicomnsvr\sasmisc\solution_data_mart\,       symlink=rmi_initdata_static.sas ,     path=&deployementRootWinTarget.\CVS\rmicmn\misc\solution_data_mart\rmi_initdata_static.sas, OS = WIN, File=Y);
           %newSymlink(pathSymlink=&deployementRootWinSource.\rmicomnsvr\sasmisc\solution_data_mart\,       symlink=rmi_initdata.sas ,     path=&deployementRootWinTarget.\CVS\rmicmn\misc\solution_data_mart\rmi_initdata.sas, OS = WIN, File=Y);
           %newSymlink(pathSymlink=&deployementRootWinSource.\rmicomnsvr\sasmisc\solution_data_mart\,       symlink=rmi_initddl_mapping.sas ,     path=&deployementRootWinTarget.\CVS\rmicmn\misc\solution_data_mart\rmi_initddl_mapping.sas, OS = WIN, File=Y);
           %newSymlink(pathSymlink=&deployementRootWinSource.\rmicomnsvr\sasmisc\solution_data_mart\,       symlink=rmi_initddl_mart.sas ,     path=&deployementRootWinTarget.\CVS\rmicmn\misc\solution_data_mart\rmi_initddl_mart.sas, OS = WIN, File=Y);
           %newSymlink(pathSymlink=&deployementRootWinSource.\rmicomnsvr\sasmisc\solution_data_mart\,       symlink=rmi_initddl_staging.sas ,     path=&deployementRootWinTarget.\CVS\rmicmn\misc\solution_data_mart\rmi_initddl_staging.sas, OS = WIN, File=Y);
           %newSymlink(pathSymlink=&deployementRootWinSource.\rmicomnsvr\sasmisc\solution_data_mart\,       symlink=rmi_initddl_static.sas ,     path=&deployementRootWinTarget.\CVS\rmicmn\misc\solution_data_mart\rmi_initddl_static.sas, OS = WIN, File=Y);
           %newSymlink(pathSymlink=&deployementRootWinSource.\rmicomnsvr\sasmisc\solution_data_mart\,       symlink=create_rd_environments.sas ,     path=&deployementRootWinTarget.\CVS\rmicmn\misc\solution_data_mart\create_rd_environments.sas, OS = WIN, File=Y);
           %newSymlink(pathSymlink=&deployementRootWinSource.\rmicomnsvr\sasmisc\solution_data_mart\,       symlink=create_solution_data_mart.sas ,     path=&deployementRootWinTarget.\CVS\rmicmn\misc\solution_data_mart\create_solution_data_mart.sas, OS = WIN, File=Y);
 
           %newSymlink(pathSymlink=&deployementRootWinSource.\rmifirmmva\,     symlink=sasmisc ,     path=&deployementRootWinTarget.\CVS\rmifra\misc, OS = WIN);
           %newSymlink(pathSymlink=&deployementRootWinSource.\rmilifemva\,     symlink=sasmisc ,     path=&deployementRootWinTarget.\CVS\rmilife\misc, OS = WIN);
           %newSymlink(pathSymlink=&deployementRootWinSource.\rmipcmva\,       symlink=sasmisc ,       path=&deployementRootWinTarget.\CVS\rmiprpcs\misc, OS = WIN);
           %newSymlink(pathSymlink=&deployementRootWinSource.\rmimktmva\,      symlink=sasmisc,      path=&deployementRootWinTarget.\CVS\rmimkrsk\misc, OS  = WIN);
 
           %newSymlink(pathSymlink=&deployementRootWinSource.\rmicomnsvr\sasmacro\,   symlink= rmietlin.sas,   path=&deployementRootWinTarget.\CVS\shell\auto\en\rmietlin.sas, OS = WIN, File=Y);
           %newSymlink(pathSymlink=&deployementRootWinSource.\rmicomnsvr\sasmacro\,   symlink= rmiinit.sas,    path=&deployementRootWinTarget.\CVS\shell\auto\en\rmiinit.sas, OS = WIN, File=Y);
 
           %newSymlink(pathSymlink=D:\SAS\config\Lev1\SASApp\SASEnvironment\,   symlink=SASMacro,    path=&deployementRootWinTarget.\CVS\rmsrvcmn\ucmacros\en, OS = WIN);
 
           %newSymlink(pathSymlink=&deployementRootWinSource.\rskrptmrtvrt\,   symlink= ucmacros,   path=&deployementRootWinTarget.\CVS\riskrpt\ucmacros\en\, OS = WIN);
           %newSymlink(pathSymlink=&deployementRootWinSource.\rskrptmrtvrt\,       symlink= sasmisc,   path=&deployementRootWinTarget.\CVS\riskrpt\misc\, OS = WIN);
           %newSymlink(pathSymlink=&deployementRootWinSource.\rskrptmrtvrt\sasmacro\,   symlink= rrrinit.sas,    path=&deployementRootWinTarget.\CVS\shell\auto\en\rrrinit.sas, OS = WIN, File=Y);
 
            libname out "D:\Program Files\SAS\SASFoundation\9.2\rmicomnsvr\sasmisc\sashelp"; 
            %smd2ds(dir=&deployementRootWinTarget.\CVS\rmicmn\smd\, lib=OUT, basename=rmilabels);
            %smd2ds(dir=&deployementRootWinTarget.\CVS\rmicmn\smd\, lib=OUT, basename=rmiutilmsg);
            %smd2ds(dir=&deployementRootWinTarget.\CVS\rmicmn\smd\, lib=OUT, basename=rmicalcmsg);
 
 
%mend;
 
 
 
%macro unixToWindows (deployementRootWin=, deployementRootUnix=, CVS_like = 1);
     %if &CVS_like %then %do;
           %newSymlink(pathSymlink=&deployementRootUnix./ucmacros/,   symlink= rmicomnsvr,     path=&deployementRootWin./CVS/rmicmn/ucmacros/en/);
           %newSymlink(pathSymlink=&deployementRootUnix./ucmacros/,   symlink= rmifirmmva,     path=&deployementRootWin./CVS/rmifra/ucmacros/en/);
           %newSymlink(pathSymlink=&deployementRootUnix./ucmacros/,   symlink= rmilifemva,     path=&deployementRootWin./CVS/rmilife/ucmacros/en/);
           %newSymlink(pathSymlink=&deployementRootUnix./ucmacros/,   symlink= rmipcmva,       path=&deployementRootWin./CVS/rmiprpcs/ucmacros/en/);
           %newSymlink(pathSymlink=&deployementRootUnix./ucmacros/,   symlink= rmimktmva,      path=&deployementRootWin./CVS/rmimkrsk/ucmacros/en/);
 
           %newSymlink(pathSymlink=&deployementRootUnix./sasstp/,     symlink= rmicomnsvr,     path=&deployementRootWin./CVS/rmicmn/stp/en/);
           %newSymlink(pathSymlink=&deployementRootUnix./sasstp/,     symlink= rmifirmmva,     path=&deployementRootWin./CVS/rmifra/stp/en/);
           %newSymlink(pathSymlink=&deployementRootUnix./sasstp/,     symlink= rmilifemva,     path=&deployementRootWin./CVS/rmilife/stp/en/);
           %newSymlink(pathSymlink=&deployementRootUnix./sasstp/,     symlink= rmipcmva,       path=&deployementRootWin./CVS/rmiprpcs/stp/en/);
           %newSymlink(pathSymlink=&deployementRootUnix./sasstp/,     symlink= rmimktmva,      path=&deployementRootWin./CVS/rmimkrsk/stp/en/);
 /*
           %createDir(path=&deployementRootUnix./misc/rmicomnsvr/);
*/           %createDir(path=&deployementRootUnix./misc/rmicomnsvr/solution_data_mart);
 
           %newSymlink(pathSymlink=&deployementRootUnix./misc/rmicomnsvr/,       symlink=system_data , path=&deployementRootWin./CVS/rmicmn/misc/system_data/);
           %newSymlink(pathSymlink=&deployementRootUnix./misc/rmicomnsvr/,       symlink=reportmart ,path=&deployementRootWin./CVS/rmicmn/misc/reportmart/);
           %newSymlink(pathSymlink=&deployementRootUnix./misc/rmicomnsvr/,       symlink=rptimages , path=&deployementRootWin./CVS/rmicmn/misc/rptimages/);
           %newSymlink(pathSymlink=&deployementRootUnix./misc/rmicomnsvr/,       symlink=batch,      path=&deployementRootWin./CVS/rmicmn/misc/batch/);
           %newSymlink(pathSymlink=&deployementRootUnix./misc/rmicomnsvr/,       symlink=martddl ,   path=&deployementRootWin./CVS/rmicmn/misc/martddl/);
           %newSymlink(pathSymlink=&deployementRootUnix./misc/rmicomnsvr/,       symlink=Config ,    path=&deployementRootWin./CVS/rmicmn/misc/Config/);
           %newSymlink(pathSymlink=&deployementRootUnix./misc/rmicomnsvr/solution_data_mart/,       symlink=rd_env ,     path=&deployementRootWin./CVS/rmicmn/misc/solution_data_mart/rd_env/);
           %newSymlink(pathSymlink=&deployementRootUnix./misc/rmicomnsvr/solution_data_mart/,       symlink=rmi_env ,     path=&deployementRootWin./CVS/rmicmn/misc/solution_data_mart/rmi_env/);
           %newSymlink(pathSymlink=&deployementRootUnix./misc/rmicomnsvr/solution_data_mart/,       symlink=multi_app ,     path=&deployementRootWin./CVS/rmicmn/misc/solution_data_mart/multi_app/);
           %newSymlink(pathSymlink=&deployementRootUnix./misc/rmicomnsvr/solution_data_mart/,       symlink=sampledata ,     path=&deployementRootWin./CVS/rmicmn/misc/solution_data_mart/sampledata/);
            %newSymlink(pathSymlink=&deployementRootUnix./misc/rmicomnsvr/solution_data_mart/,       symlink=sampledata_append ,     path=&deployementRootWin./CVS/rmicmn/misc/solution_data_mart/sampledata_append/);

           %newSymlink(pathSymlink=&deployementRootUnix./misc/rmicomnsvr/solution_data_mart/,       symlink=env_rsk ,     path=&deployementRootWin./CVS/rmsrvcmn/misc/solution_data_mart/env_rsk);
 
           %newSymlink(pathSymlink=&deployementRootUnix./misc/rmicomnsvr/solution_data_mart/,       symlink=rmi_create_sample_data.sas ,     path=&deployementRootWin./CVS/rmicmn/misc/solution_data_mart/rmi_create_sample_data.sas);
           %newSymlink(pathSymlink=&deployementRootUnix./misc/rmicomnsvr/solution_data_mart/,       symlink=rmi_create_solution_data_mart.sas ,     path=&deployementRootWin./CVS/rmicmn/misc/solution_data_mart/rmi_create_solution_data_mart.sas);
           %newSymlink(pathSymlink=&deployementRootUnix./misc/rmicomnsvr/solution_data_mart/,       symlink=rmi_add_new_configuration_set.sas ,     path=&deployementRootWin./CVS/rmicmn/misc/solution_data_mart/rmi_add_new_configuration_set.sas);
           %newSymlink(pathSymlink=&deployementRootUnix./misc/rmicomnsvr/solution_data_mart/,       symlink=rmi_initdata_mapping.sas ,     path=&deployementRootWin./CVS/rmicmn/misc/solution_data_mart/rmi_initdata_mapping.sas);
           %newSymlink(pathSymlink=&deployementRootUnix./misc/rmicomnsvr/solution_data_mart/,       symlink=rmi_initdata_mart.sas ,     path=&deployementRootWin./CVS/rmicmn/misc/solution_data_mart/rmi_initdata_mart.sas);
           %newSymlink(pathSymlink=&deployementRootUnix./misc/rmicomnsvr/solution_data_mart/,       symlink=rmi_initdata_staging.sas ,     path=&deployementRootWin./CVS/rmicmn/misc/solution_data_mart/rmi_initdata_staging.sas);
           %newSymlink(pathSymlink=&deployementRootUnix./misc/rmicomnsvr/solution_data_mart/,       symlink=rmi_initdata_static.sas ,     path=&deployementRootWin./CVS/rmicmn/misc/solution_data_mart/rmi_initdata_static.sas);
           %newSymlink(pathSymlink=&deployementRootUnix./misc/rmicomnsvr/solution_data_mart/,       symlink=rmi_initdata.sas ,     path=&deployementRootWin./CVS/rmicmn/misc/solution_data_mart/rmi_initdata.sas);
           %newSymlink(pathSymlink=&deployementRootUnix./misc/rmicomnsvr/solution_data_mart/,       symlink=rmi_initddl_mapping.sas ,     path=&deployementRootWin./CVS/rmicmn/misc/solution_data_mart/rmi_initddl_mapping.sas);
           %newSymlink(pathSymlink=&deployementRootUnix./misc/rmicomnsvr/solution_data_mart/,       symlink=rmi_initddl_mart.sas ,     path=&deployementRootWin./CVS/rmicmn/misc/solution_data_mart/rmi_initddl_mart.sas);
           %newSymlink(pathSymlink=&deployementRootUnix./misc/rmicomnsvr/solution_data_mart/,       symlink=rmi_initddl_staging.sas ,     path=&deployementRootWin./CVS/rmicmn/misc/solution_data_mart/rmi_initddl_staging.sas);
           %newSymlink(pathSymlink=&deployementRootUnix./misc/rmicomnsvr/solution_data_mart/,       symlink=rmi_initddl_static.sas ,     path=&deployementRootWin./CVS/rmicmn/misc/solution_data_mart/rmi_initddl_static.sas);
           %newSymlink(pathSymlink=&deployementRootUnix./misc/rmicomnsvr/solution_data_mart/,       symlink=create_rd_environments.sas ,     path=&deployementRootWin./CVS/rmicmn/misc/solution_data_mart/create_rd_environments.sas);
 
           %newSymlink(pathSymlink=&deployementRootUnix./misc/,       symlink= rmifirmmva,     path=&deployementRootWin./CVS/rmifra/misc);
           %newSymlink(pathSymlink=&deployementRootUnix./misc/,       symlink= rmilifemva,     path=&deployementRootWin./CVS/rmilife/misc);
           %newSymlink(pathSymlink=&deployementRootUnix./misc/,       symlink= rmipcmva,       path=&deployementRootWin./CVS/rmiprpcs/misc);
           %newSymlink(pathSymlink=&deployementRootUnix./misc/,       symlink= rmimktmva,      path=&deployementRootWin./CVS/rmimkrsk/misc);
 
           %newSymlink(pathSymlink=&deployementRootUnix./sasautos/,   symlink= rmietlin.sas,   path=&deployementRootWin./CVS/shell/auto/en/rmietlin.sas);
           %newSymlink(pathSymlink=&deployementRootUnix./sasautos/,   symlink= rmiinit.sas,    path=&deployementRootWin./CVS/shell/auto/en/rmiinit.sas);
 
           %newSymlink(pathSymlink=/sas/config/Lev1/SASApp/SASEnvironment/,   symlink=SASMacro,    path=&deployementRootWin./CVS/rmsrvcmn/ucmacros/en);
 
 /* 
           Taken out - CVS doesn't have these data sets. Will be replaced with code to materialize these datasets
           %newSymlink(pathSymlink=&deployementRootUnix./sashelp/,    symlink= rmicalcmsg.sas7bdat,      path=&deployementRootWin./rmicomnsvr/sashelp/rmicalcmsg.sas7bdat);
           %newSymlink(pathSymlink=&deployementRootUnix./sashelp/,    symlink= rmilabels.sas7bdat,      path=&deployementRootWin./rmicomnsvr/sashelp/rmilabels.sas7bdat);
           %newSymlink(pathSymlink=&deployementRootUnix./sashelp/,    symlink= rmiutilmsg.sas7bdat,      path=&deployementRootWin./rmicomnsvr/sashelp/rmiutilmsg.sas7bdat);
           %newSymlink(pathSymlink=&deployementRootUnix./sashelp/,    symlink= rmicalcmsg.sas7bndx,      path=&deployementRootWin./rmicomnsvr/sashelp/rmicalcmsg.sas7bndx);
           %newSymlink(pathSymlink=&deployementRootUnix./sashelp/,    symlink= rmilabels.sas7bndx,      path=&deployementRootWin./rmicomnsvr/sashelp/rmilabels.sas7bndx);
           %newSymlink(pathSymlink=&deployementRootUnix./sashelp/,    symlink= rmiutilmsg.sas7bndx,      path=&deployementRootWin./rmicomnsvr/sashelp/rmiutilmsg.sas7bndx);
 */
           %newSymlink(pathSymlink=&deployementRootUnix./ucmacros/,   symlink= rskrptmrtvrt,   path=&deployementRootWin./CVS/riskrpt/ucmacros/en/);
           %newSymlink(pathSymlink=&deployementRootUnix./misc/,       symlink= rskrptmrtvrt,   path=&deployementRootWin./CVS/riskrpt/misc/);
           %newSymlink(pathSymlink=&deployementRootUnix./sasautos/,   symlink= rrrinit.sas,    path=&deployementRootWin./CVS/shell/auto/en/rrrinit.sas);
 
            libname out "/sas/software/SASHome/SASFoundation/9.2/sashelp"; 
            %smd2ds(dir=&deployementRootWin./CVS/rmicmn/smd/, lib=OUT, basename=rmilabels);
            %smd2ds(dir=&deployementRootWin./CVS/rmicmn/smd/, lib=OUT, basename=rmiutilmsg);
            %smd2ds(dir=&deployementRootWin./CVS/rmicmn/smd/, lib=OUT, basename=rmicalcmsg);
 
 
     %end;
     %else %do;
           %newSymlink(pathSymlink=&deployementRootUnix./ucmacros/,   symlink= rmicomnsvr,     path=&deployementRootWin./rmicomnsvr/ucmacros/);
           %newSymlink(pathSymlink=&deployementRootUnix./ucmacros/,   symlink= rmifirmmva,     path=&deployementRootWin./rmifirmmva/ucmacros/);
           %newSymlink(pathSymlink=&deployementRootUnix./ucmacros/,   symlink= rmilifemva,     path=&deployementRootWin./rmilifemva/ucmacros/);
           %newSymlink(pathSymlink=&deployementRootUnix./ucmacros/,   symlink= rmipcmva,       path=&deployementRootWin./rmipcmva/ucmacros/);
           %newSymlink(pathSymlink=&deployementRootUnix./ucmacros/,   symlink= rmimktmva,      path=&deployementRootWin./rmimktmva/ucmacros/);
 
           %newSymlink(pathSymlink=&deployementRootUnix./sasstp/,          symlink= rmicomnsvr, path=&deployementRootWin./rmicomnsvr/sasstp/);
           %newSymlink(pathSymlink=&deployementRootUnix./sasstp/,          symlink= rmifirmmva, path=&deployementRootWin./rmifirmmva/sasstp/);
           %newSymlink(pathSymlink=&deployementRootUnix./sasstp/,          symlink= rmilifemva, path=&deployementRootWin./rmilifemva/sasstp/);
           %newSymlink(pathSymlink=&deployementRootUnix./sasstp/,          symlink= rmipcmva,         path=&deployementRootWin./rmipcmva/sasstp/);
           %newSymlink(pathSymlink=&deployementRootUnix./sasstp/,          symlink= rmimktmva,        path=&deployementRootWin./rmimktmva/sasstp/);
 
           %newSymlink(pathSymlink=&deployementRootUnix./misc/,       symlink= rmicomnsvr,     path=&deployementRootWin./rmicomnsvr/sasmisc/);
           %newSymlink(pathSymlink=&deployementRootUnix./misc/,       symlink= rmifirmmva,     path=&deployementRootWin./rmifirmmva/sasmisc/);
           %newSymlink(pathSymlink=&deployementRootUnix./misc/,       symlink= rmilifemva,     path=&deployementRootWin./rmilifemva/sasmisc/);
           %newSymlink(pathSymlink=&deployementRootUnix./misc/,       symlink= rmipcmva,       path=&deployementRootWin./rmipcmva/sasmisc/);
           %newSymlink(pathSymlink=&deployementRootUnix./misc/,       symlink= rmimktmva,      path=&deployementRootWin./rmimktmva/sasmisc/);
 
           %newSymlink(pathSymlink=&deployementRootUnix./sasautos/,   symlink= rmietlin.sas,   path=&deployementRootWin./rmicomnsvr/sasmacro/rmietlin.sas);
           %newSymlink(pathSymlink=&deployementRootUnix./sasautos/,   symlink= rmiinit.sas,    path=&deployementRootWin./rmicomnsvr/sasmacro/rmiinit.sas);
 
           %newSymlink(pathSymlink=&deployementRootUnix./sashelp/,    symlink= rmicalcmsg.sas7bdat,      path=&deployementRootWin./rmicomnsvr/sashelp/rmicalcmsg.sas7bdat);
           %newSymlink(pathSymlink=&deployementRootUnix./sashelp/,    symlink= rmilabels.sas7bdat,      path=&deployementRootWin./rmicomnsvr/sashelp/rmilabels.sas7bdat);
          %newSymlink(pathSymlink=&deployementRootUnix./sashelp/,    symlink= rmiutilmsg.sas7bdat,      path=&deployementRootWin./rmicomnsvr/sashelp/rmiutilmsg.sas7bdat);
           %newSymlink(pathSymlink=&deployementRootUnix./sashelp/,    symlink= rmicalcmsg.sas7bndx,      path=&deployementRootWin./rmicomnsvr/sashelp/rmicalcmsg.sas7bndx);
           %newSymlink(pathSymlink=&deployementRootUnix./sashelp/,    symlink= rmilabels.sas7bndx,      path=&deployementRootWin./rmicomnsvr/sashelp/rmilabels.sas7bndx);
           %newSymlink(pathSymlink=&deployementRootUnix./sashelp/,    symlink= rmiutilmsg.sas7bndx,      path=&deployementRootWin./rmicomnsvr/sashelp/rmiutilmsg.sas7bndx);
 
 
 
 
           %newSymlink(pathSymlink=&deployementRootUnix./ucmacros/,   symlink= rskrptmrtvrt,   path=&deployementRootWin./rskrptmrtvrt/ucmacros/);
           %newSymlink(pathSymlink=&deployementRootUnix./misc/,       symlink= rskrptmrtvrt,   path=&deployementRootWin./rskrptmrtvrt/sasmisc/);
           %newSymlink(pathSymlink=&deployementRootUnix./sasautos/,   symlink= rrrinit.sas,    path=&deployementRootWin./rskrptmrtvrt/sasmacro/rrrinit.sas);
     %end;
%mend;
 options mprint mlogic sgen;
 
 
/*%unixToWindows (deployementRootWin=/mnt/hgfs/Phoenix/Master/Phoenix_RMI21M1_WINx64_SAS92 ,
     deployementRootUnix=/usr/local/software/SAS/SASFoundation/9.2);
%unixToWindows (deployementRootWin=/media/psf/Home/Documents/Documents/Projects/SAS/PRJ_041     (Phoenix, Int)/phoenix_rmi21m1_f90011,
     deployementRootUnix=/sas/software/SASHome/SASFoundation/9.2, CVS_Like=1);*/
/*%unixToWindows (deployementRootWin=/mnt/hgfs/Phoenix/Master/HF11_LAX_david/_windows_Like ,
     deployementRootUnix=/usr/local/software/SAS/SASFoundation/9.2);*/
 
 
%windowsToWindows (deployementRootWinSource=D:\Program Files\SAS\SASFoundation\9.2,
     deployementRootWinTarget=C:\Users\sas\Documents\phoenix_rmi21m1_f90011, CVS_Like=1);
