
%macro makeSymlink(pathSymLink= ,path=, symLink=, OS=LAX);
     %if &OS = LAX %then %do;
           X cd &pathSymLink.;
           X ln -s "&path." &symLink.;
     %end;
     %else %if &OS = WIN %then %do;
           X cd &pathSymLink.;
           %if %sysfunc(find(&symlink., ".")) %then %do;
                X mklink &symlink. &path.;
           %end;
           %else %do;
                X mklink -D &symlink. &path.;
           %end;
     %end;
%mend;
%macro delSymLink(pathSymLink =, symLink=, OS=LAX);
     %if &OS = LAX %then %do;
           X cd &pathSymLink.;
           X rm &symLink.;
     %end;
     %else %if &OS = WIN %then %do;
           X cd &pathSymLink.;
           %if %sysfunc(find(&symlink., ".")) %then %do;
                X del &symlink.;
           %end;
           %else %do;
                X rmdir &symlink.;
           %end;
     %end;
%mend;
 
%macro createDir(path=, OS=LAX);
     %if &OS = LAX %then %do;
           X mkdir &path.;
     %end;
     %else %if &OS = WIN %then %do;
     %end;
%mend;
%macro newSymlink(pathSymLink= ,path=, symLink=, OS = LAX);
     %delSymLink(pathSymlink=&pathSymlink., symlink=&symlink., OS = &OS.);
     %makeSymlink(pathSymLink=&pathSymLink. ,path=&path., symLink=&symLink., OS = &OS.);
%mend;
%macro windowsToWindows (deployementRootWinSource=, deployementRootWinTarget=);
     %newSymlink(pathSymlink=&deployementRootWinSource.\, symlink= rmicomnsvr,      path=&deployementRootWinTarget.\rmicomnsvr\, OS = WIN);
     %newSymlink(pathSymlink=&deployementRootWinSource.\, symlink= rmifirmmva,      path=&deployementRootWinTarget.\rmifirmmva\, OS = WIN);
     %newSymlink(pathSymlink=&deployementRootWinSource.\, symlink= rmilifemva,      path=&deployementRootWinTarget.\rmilifemva\, OS = WIN);
     %newSymlink(pathSymlink=&deployementRootWinSource.\, symlink= rmipcmva,            path=&deployementRootWinTarget.\rmipcmva\, OS = WIN);
     %newSymlink(pathSymlink=&deployementRootWinSource.\, symlink= rmimktmva,      path=&deployementRootWinTarget.\rmimktmva\, OS = WIN);
     %newSymlink(pathSymlink=&deployementRootWinSource.\, symlink= rskrptmrtvrt,   path=&deployementRootWinTarget.\rskrptmrtvrt\, OS = WIN);
%mend;
 
 
%macro unixToWindows (deployementRootWin=, deployementRootUnix=, CVS_like = 1);
     %if &CVS_like %then %do;
           %newSymlink(pathSymlink=&deployementRootUnix./ucmacros/,   symlink= rmicomnsvr,     path=&deployementRootWin./CVS/rmicmn/ucmacros/en/);
           %newSymlink(pathSymlink=&deployementRootUnix./ucmacros/,   symlink= rmifirmmva,     path=&deployementRootWin./CVS/rmifra/ucmacros/en/);
           %newSymlink(pathSymlink=&deployementRootUnix./ucmacros/,   symlink= rmilifemva,     path=&deployementRootWin./CVS/rmilife/ucmacros/en/);
           %newSymlink(pathSymlink=&deployementRootUnix./ucmacros/,   symlink= rmipcmva,       path=&deployementRootWin./CVS/rmiprpcs/ucmacros/en/);
           %newSymlink(pathSymlink=&deployementRootUnix./ucmacros/,   symlink= rmimktmva,      path=&deployementRootWin./CVS/rmimkrsk/ucmacros/en/);
           %newSymlink(pathSymlink=&deployementRootUnix./sasstp/,     symlink= rmicomnsvr,     path=&deployementRootWin./CVS/rmicmn/stp/en/);
           %newSymlink(pathSymlink=&deployementRootUnix./sasstp/,     symlink= rmifirmmva,     path=&deployementRootWin./CVS/rmifra/stp/en/);
           %newSymlink(pathSymlink=&deployementRootUnix./sasstp/,     symlink= rmilifemva,     path=&deployementRootWin./CVS/rmilife/stp/en/);
           %newSymlink(pathSymlink=&deployementRootUnix./sasstp/,     symlink= rmipcmva,       path=&deployementRootWin./CVS/rmiprpcs/stp/en/);
           %newSymlink(pathSymlink=&deployementRootUnix./sasstp/,     symlink= rmimktmva,      path=&deployementRootWin./CVS/rmimkrsk/stp/en/);
           %createDir(path=&deployementRootUnix./misc/rmicomnsvr);
           %createDir(path=&deployementRootUnix./misc/rmicomnsvr/solution_data_mart);
 
           %newSymlink(pathSymlink=&deployementRootUnix./misc/rmicomnsvr/,       symlink=system_data , path=&deployementRootWin./CVS/rmicmn/misc/system_data/);
           %newSymlink(pathSymlink=&deployementRootUnix./misc/rmicomnsvr/,       symlink=reportmart ,path=&deployementRootWin./CVS/rmicmn/misc/reportmart/);
           %newSymlink(pathSymlink=&deployementRootUnix./misc/rmicomnsvr/,       symlink=rptimages , path=&deployementRootWin./CVS/rmicmn/misc/rptimages/);
           %newSymlink(pathSymlink=&deployementRootUnix./misc/rmicomnsvr/,       symlink=batch,      path=&deployementRootWin./CVS/rmicmn/misc/batch/);
           %newSymlink(pathSymlink=&deployementRootUnix./misc/rmicomnsvr/,       symlink=martddl ,   path=&deployementRootWin./CVS/rmicmn/misc/martddl/);
           %newSymlink(pathSymlink=&deployementRootUnix./misc/rmicomnsvr/,       symlink=Config ,    path=&deployementRootWin./CVS/rmicmn/misc/Config/);
           %newSymlink(pathSymlink=&deployementRootUnix./misc/rmicomnsvr/solution_data_mart/,       symlink=rd_env ,     path=&deployementRootWin./CVS/rmicmn/misc/solution_data_mart/rd_env/);
           %newSymlink(pathSymlink=&deployementRootUnix./misc/rmicomnsvr/solution_data_mart/,       symlink=rmi_env ,     path=&deployementRootWin./CVS/rmicmn/misc/solution_data_mart/rmi_env/);
           %newSymlink(pathSymlink=&deployementRootUnix./misc/rmicomnsvr/solution_data_mart/,       symlink=multi_app ,     path=&deployementRootWin./CVS/rmicmn/misc/solution_data_mart/multi_app/);
           %newSymlink(pathSymlink=&deployementRootUnix./misc/rmicomnsvr/solution_data_mart/,       symlink=sampledata ,     path=&deployementRootWin./CVS/rmicmn/misc/solution_data_mart/sampledata/);
           %newSymlink(pathSymlink=&deployementRootUnix./misc/rmicomnsvr/solution_data_mart/,       symlink=env_rsk ,     path=&deployementRootWin./CVS/rmsrvcmn/misc/solution_data_mart/env_rsk);
 
              %newSymlink(pathSymlink=&deployementRootUnix./misc/rmicomnsvr/solution_data_mart/,       symlink=create_solution_data_mart.sas ,     path=&deployementRootWin./CVS/rmicmn/misc/solution_data_mart/create_solution_data_mart.sas);
           %newSymlink(pathSymlink=&deployementRootUnix./misc/rmicomnsvr/solution_data_mart/,       symlink=rmi_create_sample_data.sas ,     path=&deployementRootWin./CVS/rmicmn/misc/solution_data_mart/rmi_create_sample_data.sas);
          %newSymlink(pathSymlink=&deployementRootUnix./misc/rmicomnsvr/solution_data_mart/,       symlink=rmi_create_solution_data_mart.sas ,     path=&deployementRootWin./CVS/rmicmn/misc/solution_data_mart/rmi_create_solution_data_mart.sas);
           %newSymlink(pathSymlink=&deployementRootUnix./misc/rmicomnsvr/solution_data_mart/,       symlink=rmi_initdata_mapping.sas ,     path=&deployementRootWin./CVS/rmicmn/misc/solution_data_mart/rmi_initdata_mapping.sas);
           %newSymlink(pathSymlink=&deployementRootUnix./misc/rmicomnsvr/solution_data_mart/,       symlink=rmi_initdata_mart.sas ,     path=&deployementRootWin./CVS/rmicmn/misc/solution_data_mart/rmi_initdata_mart.sas);
           %newSymlink(pathSymlink=&deployementRootUnix./misc/rmicomnsvr/solution_data_mart/,       symlink=rmi_initdata_staging.sas ,     path=&deployementRootWin./CVS/rmicmn/misc/solution_data_mart/rmi_initdata_staging.sas);
           %newSymlink(pathSymlink=&deployementRootUnix./misc/rmicomnsvr/solution_data_mart/,       symlink=rmi_initdata_static.sas ,     path=&deployementRootWin./CVS/rmicmn/misc/solution_data_mart/rmi_initdata_static.sas);
           %newSymlink(pathSymlink=&deployementRootUnix./misc/rmicomnsvr/solution_data_mart/,       symlink=rmi_initdata.sas ,     path=&deployementRootWin./CVS/rmicmn/misc/solution_data_mart/rmi_initdata.sas);
           %newSymlink(pathSymlink=&deployementRootUnix./misc/rmicomnsvr/solution_data_mart/,       symlink=rmi_initddl_mapping.sas ,     path=&deployementRootWin./CVS/rmicmn/misc/solution_data_mart/rmi_initddl_mapping.sas);
           %newSymlink(pathSymlink=&deployementRootUnix./misc/rmicomnsvr/solution_data_mart/,       symlink=rmi_initddl_mart.sas ,     path=&deployementRootWin./CVS/rmicmn/misc/solution_data_mart/rmi_initddl_mart.sas);
           %newSymlink(pathSymlink=&deployementRootUnix./misc/rmicomnsvr/solution_data_mart/,       symlink=rmi_initddl_staging.sas ,     path=&deployementRootWin./CVS/rmicmn/misc/solution_data_mart/rmi_initddl_staging.sas);
           %newSymlink(pathSymlink=&deployementRootUnix./misc/rmicomnsvr/solution_data_mart/,       symlink=rmi_initddl_static.sas ,     path=&deployementRootWin./CVS/rmicmn/misc/solution_data_mart/rmi_initddl_static.sas);
           %newSymlink(pathSymlink=&deployementRootUnix./misc/rmicomnsvr/solution_data_mart/,       symlink=create_rd_environments.sas ,     path=&deployementRootWin./CVS/rmicmn/misc/solution_data_mart/create_rd_environments.sas);
 
           %newSymlink(pathSymlink=&deployementRootUnix./misc/,       symlink= rmifirmmva,     path=&deployementRootWin./CVS/rmifra/misc);
           %newSymlink(pathSymlink=&deployementRootUnix./misc/,       symlink= rmilifemva,     path=&deployementRootWin./CVS/rmilife/misc);
           %newSymlink(pathSymlink=&deployementRootUnix./misc/,       symlink= rmipcmva,       path=&deployementRootWin./CVS/rmiprpcs/misc);
           %newSymlink(pathSymlink=&deployementRootUnix./misc/,       symlink= rmimktmva,      path=&deployementRootWin./CVS/rmimkrsk/misc);
           %newSymlink(pathSymlink=&deployementRootUnix./sasautos/,   symlink= rmietlin.sas,   path=&deployementRootWin./CVS/shell/auto/en/rmietlin.sas);
           %newSymlink(pathSymlink=&deployementRootUnix./sasautos/,   symlink= rmiinit.sas,    path=&deployementRootWin./CVS/shell/auto/en/rmiinit.sas);
 
 
           %newSymlink(pathSymlink=/sas/config/Lev1/SASApp/SASEnvironment/,   symlink=SASMacro,    path=&deployementRootWin./CVS/rmsrvcmn/ucmacros/en);
 
           %newSymlink(pathSymlink=&deployementRootUnix./ucmacros/,   symlink= rskrptmrtvrt,   path=&deployementRootWin./CVS/riskrpt/ucmacros/en/);
           %newSymlink(pathSymlink=&deployementRootUnix./misc/,       symlink= rskrptmrtvrt,   path=&deployementRootWin./CVS/riskrpt/misc/);
           %newSymlink(pathSymlink=&deployementRootUnix./sasautos/,   symlink= rrrinit.sas,    path=&deployementRootWin./CVS/shell/auto/en/rrinit.sas);
 
            libname out "/sas/software/SASHome/SASFoundation/9.2/sashelp";
            %smd2ds(dir=&deployementRootWin./CVS/rmicmn/smd/, lib=OUT, basename=rmilabels);
            %smd2ds(dir=&deployementRootWin./CVS/rmicmn/smd/, lib=OUT, basename=rmiutilmsg);
            %smd2ds(dir=&deployementRootWin./CVS/rmicmn/smd/, lib=OUT, basename=rmicalcmsg);
 
 
     %end;
     %else %do;
           %newSymlink(pathSymlink=&deployementRootUnix./ucmacros/,   symlink= rmicomnsvr,     path=&deployementRootWin./rmicomnsvr/ucmacros/);
           %newSymlink(pathSymlink=&deployementRootUnix./ucmacros/,   symlink= rmifirmmva,     path=&deployementRootWin./rmifirmmva/ucmacros/);
           %newSymlink(pathSymlink=&deployementRootUnix./ucmacros/,   symlink= rmilifemva,     path=&deployementRootWin./rmilifemva/ucmacros/);
           %newSymlink(pathSymlink=&deployementRootUnix./ucmacros/,   symlink= rmipcmva,       path=&deployementRootWin./rmipcmva/ucmacros/);
           %newSymlink(pathSymlink=&deployementRootUnix./ucmacros/,   symlink= rmimktmva,      path=&deployementRootWin./rmimktmva/ucmacros/);
           %newSymlink(pathSymlink=&deployementRootUnix./sasstp/,          symlink= rmicomnsvr, path=&deployementRootWin./rmicomnsvr/sasstp/);
           %newSymlink(pathSymlink=&deployementRootUnix./sasstp/,          symlink= rmifirmmva, path=&deployementRootWin./rmifirmmva/sasstp/);
           %newSymlink(pathSymlink=&deployementRootUnix./sasstp/,          symlink= rmilifemva, path=&deployementRootWin./rmilifemva/sasstp/);
           %newSymlink(pathSymlink=&deployementRootUnix./sasstp/,          symlink= rmipcmva,         path=&deployementRootWin./rmipcmva/sasstp/);
           %newSymlink(pathSymlink=&deployementRootUnix./sasstp/,          symlink= rmimktmva,        path=&deployementRootWin./rmimktmva/sasstp/);
           %newSymlink(pathSymlink=&deployementRootUnix./misc/,       symlink= rmicomnsvr,     path=&deployementRootWin./rmicomnsvr/sasmisc/);
           %newSymlink(pathSymlink=&deployementRootUnix./misc/,       symlink= rmifirmmva,     path=&deployementRootWin./rmifirmmva/sasmisc/);
           %newSymlink(pathSymlink=&deployementRootUnix./misc/,       symlink= rmilifemva,     path=&deployementRootWin./rmilifemva/sasmisc/);
           %newSymlink(pathSymlink=&deployementRootUnix./misc/,       symlink= rmipcmva,       path=&deployementRootWin./rmipcmva/sasmisc/);
           %newSymlink(pathSymlink=&deployementRootUnix./misc/,       symlink= rmimktmva,      path=&deployementRootWin./rmimktmva/sasmisc/);
           %newSymlink(pathSymlink=&deployementRootUnix./sasautos/,   symlink= rmietlin.sas,   path=&deployementRootWin./rmicomnsvr/sasmacro/rmietlin.sas);
           %newSymlink(pathSymlink=&deployementRootUnix./sasautos/,   symlink= rmiinit.sas,    path=&deployementRootWin./rmicomnsvr/sasmacro/rmiinit.sas);
           %newSymlink(pathSymlink=&deployementRootUnix./sashelp/,    symlink= rmicalcmsg.sas7bdat,      path=&deployementRootWin./rmicomnsvr/sashelp/rmicalcmsg.sas7bdat);
           %newSymlink(pathSymlink=&deployementRootUnix./sashelp/,    symlink= rmilabels.sas7bdat,      path=&deployementRootWin./rmicomnsvr/sashelp/rmilabels.sas7bdat);
          %newSymlink(pathSymlink=&deployementRootUnix./sashelp/,    symlink= rmiutilmsg.sas7bdat,      path=&deployementRootWin./rmicomnsvr/sashelp/rmiutilmsg.sas7bdat);
           %newSymlink(pathSymlink=&deployementRootUnix./sashelp/,    symlink= rmicalcmsg.sas7bndx,      path=&deployementRootWin./rmicomnsvr/sashelp/rmicalcmsg.sas7bndx);
           %newSymlink(pathSymlink=&deployementRootUnix./sashelp/,    symlink= rmilabels.sas7bndx,      path=&deployementRootWin./rmicomnsvr/sashelp/rmilabels.sas7bndx);
           %newSymlink(pathSymlink=&deployementRootUnix./sashelp/,    symlink= rmiutilmsg.sas7bndx,      path=&deployementRootWin./rmicomnsvr/sashelp/rmiutilmsg.sas7bndx);
 
 
 
           %newSymlink(pathSymlink=&deployementRootUnix./ucmacros/,   symlink= rskrptmrtvrt,   path=&deployementRootWin./rskrptmrtvrt/ucmacros/);
           %newSymlink(pathSymlink=&deployementRootUnix./misc/,       symlink= rskrptmrtvrt,   path=&deployementRootWin./rskrptmrtvrt/sasmisc/);
           %newSymlink(pathSymlink=&deployementRootUnix./sasautos/,   symlink= rrrinit.sas,    path=&deployementRootWin./rskrptmrtvrt/sasmacro/rrinit.sas);
     %end;
%mend;
options mprint mlogic sgen;
 
%unixToWindows (deployementRootWin=/home/sas/Desktop/Mount/phoenix_rmi21m1_f90011 ,
    deployementRootUnix=/sas/software/SASHome/SASFoundation/9.2,
     CVS_Like=1);
