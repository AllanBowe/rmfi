/* as per https://rmitesting.atlassian.net/browse/RMIT-1482 */

%macro recreate_report_tables(fileloc=,low_date='01Jan04:00:00:00'dt, high_date='31DEC9999:23:59:59'dt );

%include "&fileloc./report_config_data.sas" /lrecl=10000;
%include "&fileloc./report_configuration.sas" /lrecl=10000;
%include "&fileloc./report_ds_contents.sas" /lrecl=10000;
%include "&fileloc./report_ds_defs.sas" /lrecl=10000;
%include "&fileloc./report_ds_structure.sas" /lrecl=10000;
%include "&fileloc./report_fact_data_map.sas" /lrecl=10000;
%include "&fileloc./report_fact_ds_nm.sas" /lrecl=10000;
%include "&fileloc./report_layout.sas" /lrecl=10000;
%include "&fileloc./report_rr_table.sas" /lrecl=10000;
%include "&fileloc./report_var_assign.sas" /lrecl=10000;

/*
* report_config_data.sas  =  CONFIG_SET_ID, REPORT_CD, SOURCE_SYSTEM_CD, CONFIGURATION_TYPE_TXT, PROJECT_NAME
* report_configuration.sas   =  CONFIG_SET_ID   REPORT_CD
  report_ds_contents.sas     =  config_set_id   report_cd    datagroup(numeric)   order of row in report
* report_ds_defs.sas         =  CONFIG_SET_ID   REPORT_CD    DATAGROUP(numeric)   PROPERTY
* report_ds_structure.sas    =  CONFIG_SET_ID   REPORT_CD    DATAGROUP(numeric)   VAR_NO
 report_fact_data_map.sas   =  config_set_id   report_cd    datagroup(numeric)   order in the report
* report_fact_ds_nm.sas      =  CONFIG_SET_ID   FACT_DS_NM   FACT_DS_ID
* report_layout.sas          =  CONFIG_SET_ID   REPORT_CD
* report_rr_table.sas        =  CONFIG_SET_ID   REPORT_CD    REPORT_GRP           RR_TABLE_NM
* report_var_assign.sas      =  CONFIG_SET_ID   REPORT_CD    REPORT_VAR_NM
*/
 
proc sort data=report_config_data;
    by CONFIG_SET_ID REPORT_CD SOURCE_SYSTEM_CD CONFIGURATION_TYPE_TXT PROJECT_NAME;
proc sort data=report_configuration;
    by CONFIG_SET_ID REPORT_CD;
/*proc sort data=report_ds_contents;
    by CONFIG_SET_ID, REPORT_CD, SOURCE_SYSTEM_CD, CONFIGURATION_TYPE_TXT, PROJECT_NAME;*/
proc sort data=report_ds_defs;
    by CONFIG_SET_ID REPORT_CD DATAGROUP PROPERTY;
proc sort data=report_ds_structure;
    by CONFIG_SET_ID REPORT_CD DATAGROUP VAR_NO;
/*proc sort data=report_fact_data_map;
    by CONFIG_SET_ID REPORT_CD SOURCE_SYSTEM_CD, CONFIGURATION_TYPE_TXT, PROJECT_NAME;*/
proc sort data=report_fact_ds_nm;
    by CONFIG_SET_ID FACT_DS_NM FACT_DS_ID;
proc sort data=report_layout;
    by CONFIG_SET_ID REPORT_CD;
proc sort data=report_rr_table;
    by CONFIG_SET_ID REPORT_CD REPORT_GRP RR_TABLE_NM;
proc sort data=report_var_assign;
    by CONFIG_SET_ID REPORT_CD REPORT_VAR_NM;
run;

%cnvrt_ds2cards(base_ds=WORK.report_config_data, tgt_ds=WORK.report_config_data, cards_file="&fileloc./report_config_data.sas");
%cnvrt_ds2cards(base_ds=WORK.report_configuration, tgt_ds=WORK.report_configuration, cards_file="&fileloc./report_configuration.sas");
/*%cnvrt_ds2cards(base_ds=report_ds_contents, tgt_ds=report_ds_contents, cards_file="&fileloc./report_ds_contents.sas");*/
%cnvrt_ds2cards(base_ds=WORK.report_ds_defs, tgt_ds=WORK.report_ds_defs, cards_file="&fileloc./report_ds_defs.sas");
%cnvrt_ds2cards(base_ds=WORK.report_ds_structure, tgt_ds=WORK.report_ds_structure, cards_file="&fileloc./report_ds_structure.sas");
/*%cnvrt_ds2cards(base_ds=report_fact_data_map, tgt_ds=report_fact_data_map, cards_file="&fileloc./report_fact_data_map.sas");*/
%cnvrt_ds2cards(base_ds=WORK.report_fact_ds_nm, tgt_ds=WORK.report_fact_ds_nm, cards_file="&fileloc./report_fact_ds_nm.sas");
%cnvrt_ds2cards(base_ds=WORK.report_layout, tgt_ds=WORK.report_layout, cards_file="&fileloc./report_layout.sas");
%cnvrt_ds2cards(base_ds=WORK.report_rr_table, tgt_ds=WORK.report_rr_table, cards_file="&fileloc./report_rr_table.sas");
%cnvrt_ds2cards(base_ds=WORK.report_var_assign, tgt_ds=WORK.report_var_assign, cards_file="&fileloc./report_var_assign.sas");

%mend;
%recreate_report_tables(fileloc=/sas/software/SASHome/SASFoundation/9.2/misc/rmicomnsvr/solution_data_mart/sampledata/static);