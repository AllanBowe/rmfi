/* update mapping directory */
libname mapping "C:\temp\mapping";

proc sql noprint;
select distinct memname into: memlist separated by ' '
   from dictionary.tables where libname='MAPPING';

%macro looper;
%do x=1 %to 10;*%sysfunc(countw(&memlist));
   %let ds=%scan(&memlist,&x);
   data &ds; set mapping.&ds (where=(config_set_id ne 'SOLVENCY2_QIS5_FEA')); 
   if config_set_id='SOLVENCY2_QIS5' then config_set_id='SOLVENCY2_LVL2_OCT2011';
   else if config_set_id='SOLVENCY2_QIS5_FCA' then config_set_id='SOLVENCY2_CP-13';
   run;
%end;
%mend;
%looper;
