
/* two preliminary steps:

1 - update the three URLs below (RMFI root, output directory, sourcecode location)
2 - ARCHIVE the existing outputs!  see location &outlib below.
*/

%let root=/sas/config/Lev1/AppData/SASRiskManagementForInsurance/2.1; /* root location of inputs */
%let outlib=/home/sas/Desktop/Mount/Home/LocalSync/RMI/Nav/21HF13data/; /* location for outputs */
%let sourcecode= /home/sas/Desktop/Mount/rmfi/DataWeb.sas; /* THIS script! */
%let user=sas;

%let footer = '</table></br><a href=#top>Top</a></br><a href=_all_libs.htm>librefs</a></br>
            <a href=_index.htm>table index</a></html>';
%let header = '<html> <a href=_all_libs.htm> folders </a> &nbsp|&nbsp <a href=_index.htm> tables </a>
            &nbsp|&nbsp <a href=_all_cols.htm> columns </a>
            &nbsp|&nbsp <a href=_source_code.txt> Source Code</a>
            &nbsp|&nbsp '
            "Generated on %sysfunc(datetime(),datetime19.) " ;

options validvarname=any nonotes nomprint nosource ;

data _x1; /* enter a dummy libref and file location below (below &root) */
   infile cards dsd dlm=',' missover ;
   input lib :$8. loc :$500. ;
   if lib='DDS' or lib='SAS_HELP' or lib=:'PP' then call execute("libname "||lib||" '"||left(trim(loc))||"';");
   else call execute("libname "||lib||" '&root"||left(trim(loc))||"';");
cards;
x_cfg    ,/x_config
mapping  ,/indata/mapping
static   ,/indata/static
staging  ,/indata/staging
indata   ,/indata
base     ,/indata/rd_env/base
rd_base  ,/indata/rd_env/base/rmi_base_environment
base_rmi ,/indata/rd_env/base_rmi
rd_b_rmi ,/indata/rd_env/base_rmi/rmi_base_environment
fmw      ,/indata/rd_env/firmwide
rd_fmw   ,/indata/rd_env/firmwide/rmi_firmwide_environment
life     ,/indata/rd_env/life
rd_life  ,/indata/rd_env/life/rmi_life_environment
mkt      ,/indata/rd_env/market
rd_mkt   ,/indata/rd_env/market/rmi_market_environment
nl       ,/indata/rd_env/nonlife
rd_nl    ,/indata/rd_env/nonlife/rmi_nonlife_environment
API      ,/indata/rrr_api/fact
API_ERR  ,/indata/rrr_api/error
API_INT  ,/indata/rrr_api/intermediate
API_REF  ,/indata/rrr_api/reference
API_SAS  ,/indata/rrr_api/sascontrol
API_STAT ,/indata/rrr_api/status
API_USER ,/indata/rrr_api/usercontrol
ETL_CONF ,/indata/R4_Config
ETL_LAND ,/indata/R4_Land
RRR      ,/data/userdata/&user./reportmart
SYS_DATA ,/data/userdata/&user./system_data
PSD_MAP   ,/data/userdata/&user./pvt_soln_datamart/mapping
PSD_CONF  ,/data/userdata/&user./pvt_soln_datamart/static
PP0       ,/data/userdata/&user/entities/main/playpens/NONLIFE/datamart
PP1       ,/data/userdata/&user/entities/main/playpens/NONLIFE/datamart/staging
PP2       ,/data/userdata/sas/entities/main/playpens/NONLIFE/datamart/enriched_data/instdata
PP3       ,/data/userdata/sas/entities/main/playpens/NONLIFE/datamart/enriched_data/mktdata
PP4       ,/data/userdata/sas/entities/main/playpens/NONLIFE/datamart/enriched_data/parameters
;;;
run;
/*SAS_HELP ,D:/SASHome/SASFoundation/9.2/rmicomnsvr/sashelp*/
data _null_;
file "&outlib.\_source_code.txt";
infile "&sourcecode" ;
input;
put _infile_;
run;

proc sql;
create table _x2 as select a.loc, b.*
   from _x1 a
   inner join dictionary.columns b
   on upcase(a.lib)=b.libname
   where memtype='DATA'
   order by libname, memname, varnum;


data _x3;
set _x2 end=lastobs;
length varlist $32000; retain varlist;
by libname memname;
if first.memname then do;
   x+1;
   varlist='';
end;
varlist=catx(' ',varlist,name);
if last.memname then do;
   call symput(cats('lib',x),trim(libname));
   call symput(cats('loc',x),trim(loc));
   call symput(cats('mem',x),trim(memname));
   call symput(cats('varlist',x),varlist);
end;
if lastobs then call symput('totobs',x);
run;
options noquotelenmax;
%macro output;
%do x=1 %to &totobs;
   %let nvars=%sysfunc(countw(&&varlist&x));
   data _null_;
   file "&outlib\&&lib&x-&&mem&x...htm";
   if _n_=1 then do;
      put &header /
         "<title>&&mem&x</title><body>"/
         "<h2><a href='_&&lib&x...htm'>&&lib&x..</a>.&&mem&x</h2>"/
         "<h3>Located: <b> &root.&&loc&x</b> (" nobs " obs)</h3>"/
         "<table border=1 font-weight=700><tr style='background-color:PaleGreen'>";
      %do y=1 %to &nvars.;
         put "<th><a href='%scan(&&varlist&x,&y).html'>%scan(&&varlist&x,&y)</a> </th>";
      %end;
      put '</tr>';
   end;
   set &&lib&x...&&mem&x end=lastobs nobs=nobs;
   put '<tr style="background-color:lightcyan">';
   %do y=1 %to &nvars.;
      line= "<td> "||htmlencode(%scan(&&varlist&x,&y))||"</td>";
      put line;
   %end;
   put '</tr>';
   if (_n_ * &nvars.) gt 30000 or lastobs then do;
      put &footer;
      stop;
   end;
   run;
%end;
/* now for the index tables */

proc sql;
create table index1 as select a.loc,b.memname ,b.libname
   from _x1 a left join dictionary.tables b
   on upcase(a.lib)=b.libname
   where memtype='DATA'
   order by memname,libname;

   data _Null_;
   file "&outlib\_index.htm";
   if _n_=1 then do;
      put &header /
         "<title>INDATA</title><body>"/
         "<h2>List of all tables  </h2>"/
         "<table border=1 font-weight=700><tr style='background-color:PaleGreen'>";
         put "<th> TABLE</th><th>LIB</th><th>SUB-DIRECTORY</th></tr>";
   end;
   set index1 end=lastobs;
   put '<tr style="background-color:lightcyan">';
   x=cats("<td> <a href='",trim(libname),'-',trim(memname),".htm'>",trim(memname),"</a></td>");
   y=cats("<td> <a href='_",trim(libname),".htm'>",trim(libname),"</a></td>");
   put x y "<td>" loc "</td></tr>";
   if lastobs then put &footer;
   run;

%mend;
%output;

%macro lib_index(lib);
proc sort data=index1(where=(libname="%upcase(&lib)")) out=index2;
by memname;
run;
data _null_;
   set index2 end=lastobs ;
   file "&outlib\_&lib..htm";
   if _n_=1 then do;
      put &header /
         "<title>&lib library</title><body>"/
         "<h2>List of all tables in the &root<b>" loc " </b> directory </h2>"/
         "<table border=1 font-weight=700><tr style='background-color:PaleGreen'>";
         put "<th> TABLE</th></tr>";
   end;
   put '<tr style="background-color:lightcyan">';
   x=cats("<td> <a href='",trim(libname),'-',trim(memname),".htm'>",trim(memname),"</a></td></tr>");
   put x;
   if lastobs then put &footer;
   run;
%mend;

data _null_;
set _x1;
call execute('%lib_index('||lib||');');
run;
proc sort data=_x1; by loc; run;
data _null_;
   file "&outlib\_all_libs.htm";
   set _x1 end=lastobs;
   if _n_=1 then do;
      put &header /
         "<title>All Locations</title><body>"/
         "<h2>List of all queried locations </h2>"/
         "<table border=1 font-weight=700><tr style='background-color:PaleGreen'>";
         put "<th> LIBREF </th><th>Location</th></tr>";
   end;
   put '<tr style="background-color:lightcyan">';
   x=cats("<td> <a href='_",trim(lib),".htm'>",trim(lib),"</a></td><td>",loc,"</td></tr>");
   put x;
   if lastobs then put &footer;
run;

proc sql;
create table _x4 as select libname,memname,name,label, type, length, format, informat, idxusage, notnull,varnum
from dictionary.columns where memtype='DATA' and libname in
   (select upcase(lib) from _x1)
order by name, memname;

create table _x5 as select distinct name from _x4;

data _null_;
   file "&outlib\_all_cols.htm";
   set _x5 end=lastobs nobs=nobs;
   by name;
   if _n_=1 then do;
      put &header /
         "<title>All Variables</title><body>"/
         "<h2>List of all Variables (" nobs ")</h2>"/
         "<table border=1 font-weight=700><tr style='background-color:PaleGreen'>";
      put "<th>NAME</th></tr>";
   end;
   put '<tr style="background-color:lightcyan">';
   y=cats("<td> <a href='",trim(name),".html'>",trim(name),"</a></td>");
   put y ;
   if lastobs then put &footer;
run;

%macro vars(var);
data _null_;
   file "&outlib\&var..html";
   set _x4 (where=(name="&var"));
   if _n_=1 then do;
      put &header /
         "<title>&var</title><body>"/
         "<h2>List of all tables containing &var</h2>"/
         "<table border=1 font-weight=700><tr style='background-color:PaleGreen'>";
      put "<th>MEMNAME</th><th> LIBNAME </th><th>LABEL</th>"
         "<th>TYPE</th><th>LEN</th><th>FORMAT</th><th>INFORMAT</th><th>IDXUSAGE</th>"
         "<th>NOTNULL</th><th>VARNUM</th></tr>";
   end;
   put '<tr style="background-color:lightcyan">';
   y=cats("<td> <a href='",trim(libname),'-',trim(memname),".htm'>",trim(memname),"</a></td>");
   x=cats("<td> <a href='_",trim(libname),".htm'>",trim(libname),"</a></td>");
   z=cats("<td>",trim(label),"</td><td>",type,"</td><td>",length,"</td><td>",format,"</td>");
   z2=cats("<td>",informat,"</td><td>", idxusage,"</td><td>"
      , notnull,"</td><td>",varnum,"</td>");/* two steps as longer than 200 char buffer */
   put y x ;
   put z z2 "</tr>";
run;
data _null_;
   file "&outlib\&var..html" mod;
   put &footer;
run;
%mend;

data _null_;
set _x5;
call execute('%vars('||trim(name)||')');
run;

options notes mprint source;

