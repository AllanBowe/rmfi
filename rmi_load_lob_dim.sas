/*---------------------------------------------------------------------
  rmi_load_lob_dim.sas
 
  Copyright (c) 2013, SAS Institute Inc., Cary, NC, USA. All Rights Reserved
 
  PURPOSE: This macro updates LINE_OF_BUSINESS_DIM in the RRR based on values 
            provided in the MAP_LINE_OF_BUSINESS mapping table.
 
 
Any changed records are UPDATED and new records are APPENDED.

Issues:
 - records are never deleted (may not be an issue)
 - if a LOB_ID has different rollup values under different CONFIG_SETS then they will be overwritten

*--------------------------------------------------------------------------------------------*/

%macro rmi_load_lob_dim();

%local reporting_language;
proc sql noprint;
select config_value into: reporting_language from rd_conf.output_option where config_name='REPORTING_LANGUAGE';

data load_line_of_business_dim;
   set rd_map.map_line_of_business(
   /*   drop =*/
      rename= (lob_id = line_of_business_id tgt_assume_ri_flg=assume_ri_flg
               tgt_lob_type=lob_type lob_type_short_desc=lob_type_desc TGT_OBLIGATION_TYPE_CD=OBLIGATION_TYPE_CD
      ));
   format LANGUAGE_CD $3. LOB_DESC $100.;
   retain LANGUAGE_CD "&reporting_language"
      lob_desc "Business descriptions not yet supplied by append process"
      lob_usage_type_nm "S2";
   processed_dttm=datetime();
run;

libname TMP_RRR "%sysfunc(pathname(RD_RPT))";
%rmi_generic_updater(inds=LOAD_line_of_business_dim
               , outds= TMP_RRR.line_of_business_dim
               , comma_pk=%str(LINE_OF_BUSINESS_ID, LANGUAGE_CD));
libname TMP_RRR clear;

%mend;