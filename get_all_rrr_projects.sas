libname rd_rpt "C:\SAS\ConfigRMFIW64\Lev1\AppData\SASRiskManagementForInsurance\2.1\data\userdata\sasdemo\reportmart";

%macro get_all_rrr_projects(project_sk=,lib=RD_RPT);
%local memlist x memname;
proc sql noprint;
select memname into: memlist separated by ' ' from dictionary.columns
   where libname=%upcase("&lib.") and name='PROJECT_SK';
create table _all_tables (memname char(32));
%do x=1 %to %sysfunc(countw(&memlist));
   %let memname=%scan(&memlist.,&x);
   data &memname;
      set rd_rpt.&memname (where=(project_sk=&project_sk));
   data _null_;
      if y=0 then do;
         call execute("proc sql; drop table &memname.;");
      end;
      else do;
         call execute("proc sql; insert into _all_tables set memname='&memname';");
      end;
      set &memname nobs=y;
      stop;
   run;

%end;
%mend;

%get_all_rrr_projects(project_sk=1);
