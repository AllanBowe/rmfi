/*-----------------------------------------------------------------------------
  Copyright (c) 2011 by SAS Institute Inc., Cary, NC, USA.

  NAME: x_batch_create_report.sas
 
  PURPOSE: Runs report stored process in batch mode, HF6 version (based on Life Risk Capital batch macro.)

  USAGE: %x_batch_create_report(
             ENTITY=<the business entity name to use in the analysis (defaults to Main)>,
             PLAYPEN=<an RMI firmwide playpen name>,
             USERNAME=<your userid>,
             PASSWD=<your password>,
             LANGUAGE=<the locale to use for running (messages, etc.)>,
			 report=<Report code e.g. SCR-B3C TP-F1 (refer to static.report_configuration)>,
		  	 repos=<S for shared P for private>,
             );

  NOTES:
      Do not set CLEAR_WORKSPACE_FLG to N if running multiple batch processes
        in the same session of SAS

-----------------------------------------------------------------------------------*/
%macro x_batch_create_report( 
          entity=Main,   /* user must "be in" an entity */
          PLAYPEN=,
          USERNAME=, 
          PASSWD=,
          language=,
		  report=,
		  repos=P,
          CLEAR_WORKSPACE_FLG=Y);  
 
	/* declaration and assignment of necessary global macros for report stp*/
	%global report_to_run;
	%let report_to_run=&report;
    %global repository;
	%let repository=&repos;
	%global REPORT_JOB;
	%let REPORT_JOB=Y;
	%global _program;
	%let _program=RMI_%sysfunc(translate(&report.,"_","-"));
	%global task_code;
	%let task_code=&_program.;

	data _null_;
		set "%rsk_get_playpen_dir(playpen=&playpen)/datamart/rundate";
		call symputx("base_dt",rundatetime,'G');
	run;

   %rmi_batch_run_analysis_stp(stp_filename=rmi_create_report.sas,
                               config_type_code=,
                               entity=&entity,
                               PLAYPEN=&playpen,
                               USERNAME=&username,
                               PASSWD=&passwd,
                               language=&language,
                               CLEAR_WORKSPACE_FLG=&clear_workspace_flg);   

%mend;
