
%let param=PU14DIRR;
%let root=\\psf\Home\LocalSync\RMI\Nav\PU14;

%let indir=&root\&param..ddl;
%let comments=&root\&param..csv;
%let outlib=&root\&param.\; /* remember closing slash */
%let sourcecode= \\psf\Home\Documents\rmfi\ErwinNav.sas;

%let DDS_File_Name = Data Interface Data Model.erwin ;

/* ***************************************************************************************     */


%let createdby=Allan Bowe.  Based on [&dds_file_name] file. ;
%let header=<html><a href=_all_entities.htm>All Entities</a> - <a href=_all_vars.html>All Variables</a> - 
            <a href=_source_code.txt> Source Code</a>;

%let timestamp=%sysfunc(datetime(),datetime19.);
options nosource nonotes; /* faster runtime */
options noquotelenmax;
data _null_;
file "&outlib._source_code.txt";
infile "&sourcecode" ;
input;
put _infile_;
run;

data def_col; /* is not always totally successful, you may need to manually edit the CSV to get rid of special characters etc */
infile "&comments" lrecl=20000 dsd firstobs=2 termstr=crlf missover ;
input entity :$50. entity_desc: $3000. col: $50. col_desc: $3000.;
if index(entity,' "') or index(col,' "') then do;
   put "%str(W)ARNING: issue with source file";
   put "%str(W)ARNING- please reformat (eg use excel to remove speechmarks) and re-save the file accordingly..";
   put _infile_;
   put _all_;
   stop;
end;
run;
proc sort data=def_col(keep=entity:) out=def_entity nodupkey; by _all_; run;

data _null_; set def_entity; 
call symput("D_"||subpad(left(entity),1,30)
   ,translate(tranwrd(entity_desc,'""',"'"),' ','0D0A'x,' ','09'x)); /* mac vars of entity descriptions*/
run;

/* first - create the tables (is easier to create them then use dictionary tables to get attributes) */
data a ;
format str $5000. ;
infile "&indir" lrecl=5000  dlm=none;
input;
str=left(compbl(translate(_infile_,' ','0D0A'x,' ','09'x)));
if _n_=1 then do;
   /* create subfolder in WORK library for storing empty DDS tables */
   worklib=pathname('WORK');
   put worklib=;
   if fileexist(worklib||'/ERWDDL')=0 then do;
      rc=dcreate('ERWDDL',worklib);
      call execute("libname ERWDDL '"||trim(worklib)||"/ERWDDL';");
   end; 
   call execute("proc datasets library=ERWDDL kill nolist;run;");
   /* call procedure once for all sql statements - so will stop processing on first error*/
   call execute("Proc sql;");
end;
/* create table extract */
retain ex_flg 0;
if str=:'CREATE TABLE' then do;
   ex_flg=1;
   str='CREATE TABLE ERWDDL.'||substr(str,13);
end;
if ex_flg=1 then do;
   call execute(str);
   if str=');' then ex_flg=0;
end;
else output;
run;

/* next - read in indexes & constraints.  Cannot create them due to potential inconsistencies in the erwin DDL 
   (wont execute properly in SAS) */

data index_entity(keep=entity index sql) index_col(keep= index entity col)
   rel_entity(keep=relationship entity sql key parent_entity)
   rel_col(keep=relationship entity col key parent_entity);
set a;
format sql $1000.;
/* create index extract */
retain ind_flg 0;
if str=:'CREATE UNIQUE INDEX' or str=:'CREATE INDEX' then do;
   entity=scan(str,-1);
   index=scan(str,-3);
   ind_flg=1;
   sql=catx(' ',sql,str);
end;
else if ind_flg=1 then do;
   sql=catx(' ',sql,str);
   if str not in ('(',');') then do;
      col=scan(str,1);
      output index_col;
   end;
   else if str=');' then do;
      ind_flg=0;
      output index_entity;
      sql='';
   end;
end;

retain rel_flg 0;
format relationship parent_entity $30.;
if str=:'ALTER TABLE' then do;
   entity=scan(str,-1);
   rel_flg=1;
end;
if rel_flg=1 then do;
   sql=catx(' ',sql,str);
   if str=:'ADD CONSTRAINT' then do;
      if substr(str,16,11) in ('FOREIGN KEY','PRIMARY KEY')
         then relationship='No Name  ';
      else relationship=scan(str,3);

      if find(str,'FOREIGN KEY') then key='FK';
      else if find(str,'PRIMARY KEY') then key='PK';
      else stop; /* uncaptured constraint type */

      loopstr=scan(str,2,'()');
      do x=1 to countw(loopstr);
         col=scan(loopstr,x);
         if key='PK' then parent_entity='';
         else parent_entity=scan(str,-1);
         output rel_col;
      end;        
      
   end;
   if substr(str,length(str),1)=';' then do;
      rel_flg=0;
      output rel_entity;
      sql='';
   end;
end;
retain entity index sql parent_entity relationship key;
run;




proc sql undo_policy=none; 
create table entities as select memname as entity from dictionary.members where libname='ERWDDL';
create table entities as select a.entity, count(*) as index_cnt 
   from entities a left join index_entity b
   on a.entity=b.entity    group by 1;
create table entities as select a.*,sum(case when key is not null then 1 else 0 end) as pk_cnt /* should always be 1 but just to check! */
   from entities a left join rel_entity(where=(key='PK')) b
   on a.entity=b.entity    group by 1,2;
create table entities as select a.*, count(distinct b.Parent_entity)  as parent_entities 
   from entities a left join rel_entity(where=(key='FK')) b
   on a.entity=b.entity group by 1,2,3;
create table entities as select a.*, count(distinct b.entity)  as child_entities 
   from entities a left join rel_entity(where=(key='FK')) b
   on a.entity=b.parent_entity group by 1,2,3,4;
create table entities as select a.*,b.entity_desc
   from entities a left join def_entity b 
   on a.entity=b.entity;

proc sql undo_policy=none;
create table cols as select memname as entity, name as col
      , type, length as len, varnum, label, format, informat, notnull 
   from dictionary.columns where libname='ERWDDL';
create table cols as select a.*,sum(case when key='PK' then 1 else 0 end) as PK_CNT
      ,sum(case when key='FK' then 1 else 0 end) as FK_CNT
   from cols a left join rel_col b
   on a.entity=b.entity and a.col=b.col group by 1,2,3,4,5,6,7,8,9;
create table cols as select a.*,count(distinct b.col) as IDX_CNT
   from cols a left join index_col b
   on a.entity=b.entity and a.col=b.col group by 1,2,3,4,5,6,7,8,9,10,11;
create table cols as select a.*,b.col_desc
   from cols a left join def_col b
   on a.entity=b.entity and a.col=b.col 
   order by entity, pk_cnt desc, fk_cnt desc, col;


proc sql;
select count(distinct entity) into: ent_cnt from entities;
select count(distinct col) into: col_cnt from cols;
select count(*) into: all_cols from cols;

data _null_;
file "&outlib._all_entities.htm" lrecl=4000;
set entities end=lastobs;
if _n_=1 then do;
   put "&header";
   put "<h1>Total Entities - &ent_cnt</h1>";
   put "<p> Generated on &timestamp</p>";
   put "<table border=1 font-weight=700 ><tr><th>ENTITY</th><th>PK</th><th>IDX</th>";
   put "<th>PARENT</th><th>CHILD</th><th>COMMENTS</th></tr>";
end;
format out $30000.;
out=cats("<tr><td><a href=",trim(entity),".htm>",entity,"</a></td>"
   ,"<td><a href=",trim(entity),".htm#pk>",pk_cnt,"</a></td>"
   ,"<td><a href=",trim(entity),".htm#idx>",index_cnt,"</a></td>"
   ,"<td><a href=",trim(entity),".htm#pc_relationships>",parent_entities,"</a></td>"
   ,"<td><a href=",trim(entity),".htm#pc_relationships>",child_entities,"</a></td>"
   ,"<td>",entity_desc,"</td>"
   ,'</tr>');
put out;
if lastobs then do;
   put '</table></br><a href=#top>Top</a></br> '
      "<p><i>Support queries: &createdby</i></p></html>";
end;
run;

/* create entity level pages */
proc sql noprint;
select cats('<th>',upcase(name),'</th>') into: col_headers  separated by '' 
   from dictionary.columns where libname='WORK' and memname='COLS' and upcase( name) not in ('ENTITY','ENTITY_DESC','VARNUM');
%global col_headers;


%macro entity_creator(entity);
data temp;
set cols (where=(entity="&entity") );

data _null_;
set temp nobs=nobs;
file "&outlib.&entity..htm" lrecl=4000;
if _n_=1 then do;
   format x y $10000.;
   y=trim(subpad("D_&entity",1,30));
   if symexist(y) then x=symget(y);
   put "&header - "
   "<a href=#pk> Primary Key</a> - <a href=#fk> Foreign Keys</a> - "
   "<a href=#pc_relationships> Parent/Child Relationships</a> - <a href=#idx> Indexes</a> </br>"
   "<h1>Entity: &entity</h1>"
   "<p><i> " x "</i></p>"
   "<p> Generated on &timestamp..  Num of variables: " nobs "</p>"
   "<table border=1 font-weight=700 >"
   "<tr style='background-color:PaleGreen'>"
   "<th>COL</th><th>TYPE</th><th>LEN</th><th>LABEL</th><th>FORMAT</th><th>INFORMAT</th><th>NOTNULL</th>"
   "<th>PK</th><th>FK</th><th>IDX</th><th>COMMENT</th></tr>";
end;
format out $10000.;
if pk_cnt >0 then tr='<tr style="background-color:lightcyan">';
else if fk_cnt>0 then tr='<tr style="background-color:beige">';
else tr='<tr>';
if fk_cnt >0 and pk_cnt>0 then fk='<div style="background-color:beige">'||trim(fk_cnt)||'</div>';
else fk=trim(fk_cnt);
out=tr||'<td>'||
   catq('DMT','</td><td>',"<a href="||trim(col)||".html>"||trim(col)||"</a>"
      ,type,len,label,format,informat,notnull,pk_cnt,fk,idx_cnt,col_desc)
   ||'</td></tr>';
put out;
run;
/* parent child table */
proc sql; 
create table parents as select distinct parent_entity as parent
   from rel_entity where entity="&entity" and parent_entity is not null;
create table children as select distinct entity as child
   from rel_entity where Parent_entity="&entity" and entity is not null;

data _null_;
file "&outlib.&entity..htm" mod;
if 0 then set parents nobs=pnobs;
if 0 then set children nobs=cnobs;
totnobs=max(pnobs,cnobs);

if _n_=1 then put "</table><a href=#top>Top</a></br><h2><a name=pc_relationships></a> Parent / Child Entities</h2>"
   "<table border=1 font-weight=700><tr><th>PARENT</th><th>ENTITY</th><th>CHILD</th></tr>";
do i=1 to totnobs;
   if i <= pnobs then set parents;
   if i <= cnobs then set children;

   if i=1 and pnobs=0 then put "<tr><td>(None)</td>";
   else if i> pnobs then put "<tr><td></td>";
   else do;
      str=cats("<tr><td><a href='",parent,".htm#pc_relationships'>",parent,"</a></td>");
      put str ;
   end;
      
   if i=1 then put "<td>&entity</td>";
   else put "<td></td>";

   if i=1 and cnobs=0 then put "<td>(None)</td></tr>";
   else if i>cnobs then put "<td></td></tr>";
   else do;
      str=cats("<td><a href='",child,".htm#pc_relationships'>",child,"</a></td></tr>");
      put str ;
   end;
end;
run;
data _null_;file "&outlib.&entity..htm" mod; put '</table>'; 

/* SQL section */
data _null_;
file "&outlib.&entity..htm" mod;
set rel_entity(where=(entity="&entity" and key='PK'));
if _n_=1 then put "<a href=#top>Top</a></br><h2><a name=pk></a> Primary Key </h2>";
put "<a name=" relationship "></a><i>" sql "</i></br>";

data _null_;
file "&outlib.&entity..htm" mod;
set rel_entity(where=(entity="&entity" and key='FK'));
if _n_=1 then put "<a href=#top>Top</a></br><h2><a name=fk></a> Foreign Keys </h2>";
put "<a name=" relationship "></a><i>" sql "</i></br>";

data _null_;
file "&outlib.&entity..htm" mod;
set index_entity(where=(entity="&entity"));
if _n_=1 then put "<a href=#top>Top</a></br><h2><a name=idx></a> Indexes </h2>";
put "<a name=" index "></a><i>" sql "</i></br>";

data _null_;
file "&outlib.&entity..htm" mod;
put "</br><a href=#top>Top</a><p><i>Support queries: &createdby</i></p></html>";
run;
%mend;

data _null_;
set entities;
call execute('%entity_creator('||entity||");");
run;


/* column level mappings */
%macro col_creator(col);
/* filter, as is probably more efficient than querying AND filtering in order to determine number of OBS */
data col;
set cols (where=(col="&col") );

data _null_;
Format out $10000.;
set col nobs=nobs;
file "&outlib.&col..html" lrecl=5000;
if _n_=1 then put "&header - "
   "<a href=#idx> Indexes</a> - <a href=#pk> Primary Key</a> - <a href=#fk> Foreign Keys</a>"
   "<h1>Variable: &col</h1>"
   "<p> Generated on &timestamp.. Appears in <b>" nobs "</b> tables.</p>"
   "<table border=1 ><tr font-weight=700><th>ENTITY</th>&col_headers.</tr>";
out='<tr><td>'||
   catq('DMT','</td><td>',"<a href="||trim(entity)||".htm>"||trim(entity)||"</a>"
      ,col,type,len,label,format,informat,notnull,pk_cnt,fk_cnt,idx_cnt,col_desc)
   ||'</td></tr>';
put out;

/* Primary Key */
data _null_;
format out $10000.;
file "&outlib.&col..html" mod;
set rel_col(where=(col="&col" and key='PK'));
if _n_=1 then put "</table><a href=#top>Top</a></br><h2><a name=pk></a> Primary Key </h2>"
   "<table border=1 font-weight=700 ><tr><th>ENTITY</th><th>VARIABLE</th><th>RELATIONSHIP</th></tr>";
out= "<tr><td><a href="||trim(entity)||".htm>"||trim(entity)||"</a></td>"||
      "<td>"||trim(col)||"</td>"||
      "<td><a href="||trim(entity)||".htm#"||relationship||">"||trim(relationship)||"</a></td>"||
      "</tr>";
put out;

/* Foreign Keys*/
data _null_;
format out $10000.;
file "&outlib.&col..html" mod lrecl=50000;
set rel_col(where=(col="&col" and key='FK'));
if _n_=1 then put "</table><a href=#top>Top</a></br><h2><a name=fk></a> Foreign Keys </h2>"
   "<table border=1 font-weight=700 ><tr><th>PARENT_ENTITY</th><th>VARIABLE</th>"
   "<th>CHILD_ENTITY</th><th>RELATIONSHIP</th></tr>";
out= "<tr><td><a href="||trim(parent_entity)||".htm>"||trim(parent_entity)||"</a></td>"||
      "<td>"||trim(col)||"</td>"||
      "<td><a href="||trim(entity)||".htm>"||trim(entity)||"</a></td>"||
      "<td><a href="||trim(entity)||".htm#"||relationship||">"||trim(relationship)||"</a></td>"||
      "</tr>";
put out;

/* indexes */
data _null_;
format out $10000.;
file "&outlib.&col..html" mod;
set index_col(where=(col="&col"));
if _n_=1 then put "</table><a href=#top>Top</a></br><h2><a name=idx></a> Indexes </h2>"
   "<table border=1 font-weight=700 ><tr><th>ENTITY</th><th>VARIABLE</th><th>INDEX</th></tr>";
out= "<tr><td><a href="||trim(entity)||".htm>"||trim(entity)||"</a></td>"||
      "<td>"||trim(col)||"</td>"||
      "<td><a href="||trim(entity)||".htm#"||index||">"||trim(index)||"</a></td>"||
      "</tr>";
put out;
run;

data _null_;
file "&outlib.&col..html" mod;
put "</table></br><a href=#top>Top</a><p><i>Support queries: &createdby</i></p></html>";
run;
proc sql;
drop table col;
%mend;

proc sql;
create table _all_vars as select distinct col from cols order by col;
data _null_;
file "&outlib._all_vars.html";
set _all_vars end=lastobs;
if _n_=1 then put "&header"
   "<h1>Total Variables - &all_cols (unique vars - &col_cnt)</h1>"
   "<p> Generated on &timestamp</p><ol>";
format out $10000.;
out="<li><a href="||trim(col)||".html>"||trim(col)||"</a></li>";
put out;
if lastobs then put "</ol></br><a href=#top>Top</a><p><i>Support queries: &createdby</i></p><html>";
call execute('%col_creator('||col||');');
run;
quit;
