
%let inroot=C:\HF11\thg; /* NO trailing slashes */
%let inroot=Y:\Documents\phoenix_rmi21m1_f90011\CVS;
%let outroot=C:\Program Files\SAS\SASFoundation\9.2;

options nonotes nomprint;
data _null_;
  infile cards dsd;
  input INDIR : $80. OUTDIR : $80. ;
  call symput(cats('INDIR',_n_),cats("&inroot\",indir));
  call symput(cats('OUTDIR',_n_),cats("&outroot\",outdir));
  call symput('CNT',put(_n_,8.));
/* list relative folders with NO leading or trailing slashes */
cards4;
riskrpti\ucmacros\en, rskrptmrtvrt\ucmacros
riskrpti\misc\Config, rskrptmrtvrt\sasmisc\Config
riskrpti\misc\ddls, rskrptmrtvrt\sasmisc\ddls
riskrpti\misc\programs, rskrptmrtvrt\sasmisc\programs
rmicmn\misc\batch, rmicomnsvr\sasmisc\batch
rmicmn\misc\martddl\mapping, rmicomnsvr\sasmisc\martddl\mapping
rmicmn\misc\martddl\mart, rmicomnsvr\sasmisc\martddl\mart
rmicmn\misc\martddl\staging, rmicomnsvr\sasmisc\martddl\staging
rmicmn\misc\martddl\static, rmicomnsvr\sasmisc\martddl\static
rmicmn\misc\reportmart, rmicomnsvr\sasmisc\reportmart
rmicmn\misc\solution_data_mart\multi_app\functions, rmicomnsvr\sasmisc\solution_data_mart\multi_app\functions
rmicmn\misc\solution_data_mart\multi_app\methods, rmicomnsvr\sasmisc\solution_data_mart\multi_app\methods
rmicmn\misc\solution_data_mart\rmi_env, rmicomnsvr\sasmisc\solution_data_mart\rmi_env
rmicmn\misc\solution_data_mart\rmi_env\data, rmicomnsvr\sasmisc\solution_data_mart\rmi_env\data
rmicmn\misc\solution_data_mart, rmicomnsvr\sasmisc\solution_data_mart
rmicmn\misc\solution_data_mart\sampledata\mapping, rmicomnsvr\sasmisc\solution_data_mart\sampledata\mapping
rmicmn\misc\solution_data_mart\sampledata\mart, rmicomnsvr\sasmisc\solution_data_mart\sampledata\mart
rmicmn\misc\solution_data_mart\sampledata\staging, rmicomnsvr\sasmisc\solution_data_mart\sampledata\staging
rmicmn\misc\solution_data_mart\sampledata\static, rmicomnsvr\sasmisc\solution_data_mart\sampledata\static
rmicmn\misc\solution_data_mart, rmicomnsvr\sasmisc\solution_data_mart
rmicmn\misc\system_data, rmicomnsvr\sasmisc\system_data
rmicmn\stp\en, rmicomnsvr\sasstp
rmicmn\ucmacros\en, rmicomnsvr\ucmacros
rmifra\misc\batch, rmifirmmva\sasmisc\batch
rmifra\misc\solution_data_mart\fw_env, rmifirmmva\sasmisc\solution_data_mart\fw_env
rmifra\misc\solution_data_mart\fw_env\methods, rmifirmmva\sasmisc\solution_data_mart\fw_env\methods
rmifra\stp\en, rmifirmmva\sasstp
rmifra\ucmacros\en, rmifirmmva\ucmacros
rmilife\misc\batch, rmilifemva\sasmisc\batch
rmilife\misc\solution_data_mart\life_env, rmilifemva\sasmisc\solution_data_mart\life_env
rmilife\misc\solution_data_mart\life_env\data, rmilifemva\sasmisc\solution_data_mart\life_env\data
rmilife\misc\solution_data_mart\life_env\functions, rmilifemva\sasmisc\solution_data_mart\life_env\functions
rmilife\misc\solution_data_mart\life_env\methods, rmilifemva\sasmisc\solution_data_mart\life_env\methods
rmilife\stp\en, rmilifemva\sasstp
rmilife\ucmacros\en, rmilifemva\ucmacros
rmimkrsk\misc\batch, rmimktmva\sasmisc\batch
rmimkrsk\misc\solution_data_mart\mkrsk_env, rmimktmva\sasmisc\solution_data_mart\mkrsk_env
rmimkrsk\misc\solution_data_mart\mkrsk_env\data, rmimktmva\sasmisc\solution_data_mart\mkrsk_env\data
rmimkrsk\misc\solution_data_mart\mkrsk_env\functions, rmimktmva\sasmisc\solution_data_mart\mkrsk_env\functions
rmimkrsk\misc\solution_data_mart\mkrsk_env\methods, rmimktmva\sasmisc\solution_data_mart\mkrsk_env\methods
rmimkrsk\stp\en, rmimktmva\sasstp
rmimkrsk\ucmacros\en, rmimktmva\ucmacros
rmiprpcs\stp\en, rmipcmva\sasstp
rmiprpcs\ucmacros\en, rmipcmva\ucmacros
rmsrvcmn\misc\solution_data_mart\env_rsk\data, rmicomnsvr\sasmisc\solution_data_mart\env_rsk\data
rmsrvcmn\misc\solution_data_mart\env_rsk\functions, rmicomnsvr\sasmisc\solution_data_mart\env_rsk\functions
rmsrvcmn\misc\solution_data_mart\env_rsk\methods, rmicomnsvr\sasmisc\solution_data_mart\env_rsk\methods
rmsrvcmn\ucmacros\en, rmicomnsvr\ucmacros
;;;;
run;

%macro copyTextFile(in,out);
data _null_;
    infile "&in" lrecl=32767;
    file "&out" lrecl=32767;
    input;
    put _infile_;
 run;
%mend;

%macro getFilenames(location,dataset);
filename _dir_ "%bquote(&location.)";
data &dataset.(keep=memname);
  handle=dopen( '_dir_' );
  if handle > 0 then do;
    count=dnum(handle);
    do i=1 to count;
      memname=dread(handle,i);
      output;
    end;
  end;
  rc=dclose(handle);
run;
filename _dir_ clear;
%mend getFilenames;

%macro DoCopy();
%do x=1 %to &cnt;
	%getFilenames(&&indir&x,ds&x);
	data _null_;
	set ds&x;
	if upcase(scan(memname,-1))='SAS' then 
		call execute(cats('%copyTextFile(&&indir&x\',memname,",&&outdir&x\",memname,')'));
	run;
	proc sql; drop table ds&x; quit;
%end;
%mend DoCopy;

%docopy;

options notes;
