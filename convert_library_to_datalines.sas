libname map "C:\SAS\ConfigRMFIW64\Lev1\AppData\SASRiskManagementForInsurance\2.1\indata\mapping";


%macro cnvrt_lib2cards(library=,outloc= /* without trailing slash */);
%local x ds memlist;
proc sql noprint;
select distinct memname into: memlist separated by ' ' from dictionary.tables where libname="%upcase(&library)";
%do x=1 %to %sysfunc(countw(&memlist));
   %let ds=%sysfunc(lowcase(%scan(&memlist,&x)));
   %cnvrt_ds2cards(base_ds=&library..&ds,tgt_ds=WORK.&ds,cards_file="&outloc.\&ds..sas"
%end;
%mend;
%cnvrt_lib2cards(library=MAP,outloc=C:\temp\map);
