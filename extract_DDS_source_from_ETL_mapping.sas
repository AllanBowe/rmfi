data map0;
infile "C:\temp\mapping.txt" dsd dlm='09'x firstobs=2 lrecl=20000 missover ;
input Job_name:$300.
   job_desc:$300.	job_group:$300.	Transform_Name:$300.	Mapp_Type:$300.	Src_table : $300.
   Src_ColNM:$300.	Src_Col_Len:$300.	Trg_table:$300.	
   Trg_ColNM:$300.	Trg_Col_Len:$300.	Derived_Rule:$300.;
run;

proc sql;
create table map1 as 
select Trg_table,trg_colnm, mapp_type, src_table as src_table1, src_colnm as src_colnm1
from map0 where upcase(trg_table) in (".ANALYTICAL_MODEL ",	
".ANALYTICAL_MODEL_PARAMETER ",	".ANALYTICAL_MODEL_X_EMBED_OPT ",	".ANALYTICAL_MODEL_X_ENTITY ",	
".ANALYTICAL_MODEL_X_ISSUE ",	".ANALYTICAL_MODEL_X_RISK_FACTOR ",	
	".CASHFLOW_ACCOUNT",	".CASHFLOW_FRA ",	
".CASHFLOW_INSTRUMENT ",	
".CONVERSION_SCHEDULE ",		".CORRELATION_MATRIX ",
".CORRELATION_MATRIX_ELEMENT ",		".COUNTERPARTY_ASSOC ",
".COUNTERPARTY_MART ",	".COUNTERPARTY_RATINGS ",	".COUNTERPARTY_X_INTERNAL_ORG ",	
	".CREDIT_RISK_MITIGANT ",	".CREDIT_TRANSITION_MATRIX_DATA ",		
".DISCRETE_YIELD_CURVE ",	
".FINANCIAL_CONTRACT ",	".FINANCIAL_CONTRACT_ASSOC ",	".FINANCIAL_EXPOSURE",	".FINANCIAL_FUND ",	
".FORWARD_INSTRUMENT",		".FUND_INSTRUMENT ",		
".HOLIDAY_LIST ",	".INTERNAL_ORG_ASSOC ",	".INTERNAL_ORG_MART ",	".IRREGULAR_RATE_RESETS ",
".ISSUE_CASHFLOW_RECORD ",	".ISSUE_RATINGS ",		".OPTION_INSTRUMENT ",	
".OPTION_SCHEDULE ",	".PHYSICAL_ASSET ",	".QUOTE_BOND ",	
".QUOTE_CREDIT_SPREAD ",	".QUOTE_DATA",	".QUOTE_EQUITY",	".QUOTE_FX",	".QUOTE_INDEX",	".QUOTE_IR",	
".QUOTE_VOLATILITY ",		".RATE_POLICY",	
".RATING_GRADE",		".RF_CURVE_X_RF_GROUP ",	".RISK_FACTOR ",	
".RISK_FACTOR_ATTRIBUTE ",	".RISK_FACTOR_CURVE ",	".RISK_FACTOR_X_RISK_FCTR_CURVE ",
".SECURITIZATION_POOL_MART SEGMENT",	".SEGMENT_ATTRIBUTE ",	".SEGMENT_RATINGS ",	".SEGMENT_X_RISK_FACTOR ",
".SPOT_INSTRUMENT ",		".SWAP_INSTRUMENT ");

%macro loopit;
proc sql noprint;

%do x=2 %to 60; /* leave max in to prevent infinite loop */
create table map&x as
   select a.*, b.mapp_type as mapp_type&x, b.src_table as src_table&x, b.src_colnm as src_colnm&x
   from map%eval(&x-1) a
   left join map0 (where= (trg_table is not null)) b
   on a.src_table%eval(&x-1)=b.trg_table and a.src_colnm%eval(&x-1)=b.trg_colnm
      and (substr(a.src_table%eval(&x-1),1,1) ne '.' or substr(a.src_table%eval(&x-1),1,3)='.I_');
   select count(distinct src_colnm&x) into: cnt&x from map&x where src_colnm&x is not null;
   %put &&cnt&x;
   %if &&cnt&x = 0 %then %goto next;
%end;
%next:
%put x=&x;
%let tablevars=src_table&x;
%let colvars=src_colnm&x;
%do z=%eval(&x-1) %to 1 %by -1;
   %let tablevars=&tablevars.,src_table&z;
   %let colvars= &colvars, src_colnm&z;
%end;
%put &tablevars;
%put &colvars;
data FinalMap (keep=trg_: dds_:);
set &syslast;
DDS_table=coalescec(&tablevars);
DDS_column=coalescec(&colvars);
run;
proc sort data=Finalmap noduprec; by _all_; run;
proc sort data=FinalMap(keep=Dds_:) out=DDSonly nodupkey; by _all_; run;
%mend;
%loopit;
