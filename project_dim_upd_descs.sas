/* 
FILE:  project_dim_upd_descs.sas

PURPOSE: Updates the project dim table with unique descriptions.  

INPUTS: 
1 - location of project_dim dataset
2 - %create_datalines_from_dataset macro

*/

%let project_dim_dataset=work.project_dim;
%inc "C:\GIT\AB_RMFI2\create_datalines_from_dataset.sas";
%let outfile="C:\GIT\AB_RMFI2\project_dim.sas";

data unique_projects;
infile cards dsd delimiter=',';
input  PROJECT_ID  : $50.  PROJECT_ID_DESC: $1024.;
cards;
"ASSET_LIFE_RSK_SRC","Life Scenario Asset Value"
"ASSET_MKT_RSK_SRC","Scenario Asset Value"
"BALANCE_SHEET_PROJECT","Balance Sheet Project Data"
"BOND_SPRD_RSK_CAPITAL_AGGR","Bond Spread Risk Capital"
"BSCR_AGGREGATION","Basic Solvency Capital Requirement"
"CAT_HEALTH","CAT Health Project"
"CCP_RSK_CAPITAL","CCP Risk Capital"
"CD_SPRD_RSK_CAPITAL_AGGR","Credit Derivative Spread Risk Capital Aggregation"
"CONC_RSK_CAP_AGGR","Concentration Risk Capital"
"CPTY_DEF_RISK","Counterparty Default Risk Project"
"EQ_CAPITAL_AGGR","Equity Risk Aggregation"
"EXPOSURE_VALUATION","Exposure Valuation"
"FMW_MCR","Firmwide MCR (Solo) Project"
"FMW_SCR","Firmwide SCR Project"
"FX_CAPITAL_AGGR","Market Foreign Exchange Capital Aggregation"
"GROUP_ANALYSIS_PROJECT","Group Solvency Analysis Project"
"HEALTH_AGGREGATION","Correlated Health Aggregation"
"LIAB_LIFE_RSK_SRC","Life Scenario Liability Value"
"LIAB_MKT_RSK_SRC","Read in Liability Cashflow"
"Life_epifp_scenario","Life EPIFP Risk Scenario Valuation"
"NON_SLT_HEALTH","Non SLT Health Project"
"NSCR_AGGREGATION","Net Solvency Capital Requirement"
"Nonlife_epifp_scenario","Nonlife EPIFP Liability Valuation"
"OWN_FUND_PROJECT","Own funds Project"
"OWN_FUND_VARIANCE_ANALYSIS","Own Fund Variance Analysis"
"OWN_FUND_VARIANCE_ANALYSIS_C","Own Fund Variance Analysis Change"
"PRJ_CLOSED_DRVTV_TRADES","Trades for Closed Derivatives Contracts"
"PRJ_DDSRRR_IGT1","Data Management Project � IGT Report 1"
"PRJ_DDSRRR_IGT2","Data Management Project � IGT Report 2"
"PRJ_DDSRRR_IGT3","Data Management Project � IGT Report 3"
"PRJ_DDSRRR_IGT4","Data Management Project � IGT Report 4"
"PRJ_DDSRRR_RC","Data Management Project � RC"
"PRJ_DDSRRR_RE_SPV","Data Management Project � Reinsurance  SPV "
"PRJ_LIFE_RI_RECOVERABLES_IGTAdj","Life Reinsurance Recoverables Valuation IGT Adjustment"
"PRJ_NLIFE_RI_RECOVERABLES_IGTAdj","Non-Life Reinsurance Recoverables Valuation IGT Adjustment"
"PRJ_RRR_F3A","Data Management Project � TP F3A Report "
"PRJ_RRR_F3B","Data Management Project � TP � F3B Report 4"
"PROJECT_C1B","Project for BS-C1B Report"
"PROJECT_INVESTMENT_FUND_ASSETS","Project for Investment Fund Assets"
"PROJECT_LIFE_PREMIUM_HISTORY","Life Historic Premium Project"
"PROJECT_OWN_FUND_PARTCP","Participations"
"PROJECT_VALUATION_LIABILITIES_NL","Nonlife Liability Valuation"
"PROJ_INSUR_EXPENSE_COVER","Insurer Expense Cover-A1 Reports"
"PROJ_INSUR_EXP_REV","Insurer Expense Country K1 Report"
"PROJ_LIFE_RI_RECOVERABLES","Life Reinsurance Recoverables Valuation"
"PROJ_NONLIFE_RI_RECOVERABLES","Non-Life Reinsurance Recoverables Valuation"
"PROJ_SLTH_RI_RECOVERABLES","SLTH Reinsurance Recoverables Valuation"
"Prj_Val_Assets_MKT_MTM_IGTAdj","Market Asset Mark-To-Market Valuation IGT Adjustment"
"Prj_Val_Assets_MKT_SCN_IGTAdj","Asset Scenario Valuation IGT Adjustment"
"Prj_Val_Reins_life_LIFE_SCEN","Life Cashflow Scenario of Reinsurance (with Mitigation_id -for Hyp Life SCR)"
"Prj_Val_Reins_slth_LIFE_SCEN","Health Life Cashflow Life Scenario of Reinsurance (with Mitigation_id -for Hyp SLTH SCR)"
"Prj_Val_asset_life_SLTHEALTH_SCEN","Life Policy SLT HEALTH Asset Valuation"
"Prj_Val_liab_life_LF_SCN_IGTAdj","Life Policy Life risk Scenario Valuation IGT Adjustment"
"Prj_Val_liab_life_LIFE_SCEN","Life Policy Life Risk Scenario Valuation"
"Prj_Val_liab_life_MKT_SCEN","Life Policy Market Risk Scenario Valuation"
"Prj_Val_liab_life_MKT_SCN_IGTAdj","Life Policy Market Risk Scenario Valuation IGT Adjustment"
"Prj_Val_liab_life_MTM","Life MTM Valuation Analysis"
"Prj_Val_liab_life_SLTHEALTH_SCEN","Life Policy SLT HEALTH Risk Scenario Valuation"
"Proj_insur_nl_loss_aggr","Insurer Loss Country K1 Report"
"Proj_insur_revenue","Insurer Revenue Country K1 Report"
"Proj_nonlife_uw_risk_exp","Underwriting Risks TP-E7A Report"
"Project_Claim_Size","TP-E6 Report"
"Project_Der_Assets_MKT_SCEN","Asset Scenario of Derivatives (Per Exposure_id -for Hyp Mkt SCR)"
"Project_HEALTH_AGGREGATION","Health Aggregation"
"Project_HEALTH_CATASTROPHE","Health Catastrophe Risk"
"Project_Health_Cat_Peril","Project Health Catastrophe Peril input"
"Project_Health_Cat_Rm","Health Catastrophe Risk Mitigation"
"Project_Health_Cat_SCR","Project Health Catastrophe SCR Input"
"Project_Hyp_SCR_LIFE","Hypothetical Life SCR"
"Project_Hyp_SCR_MKT","Hypothetical Market SCR Net of IGT"
"Project_Hyp_SCR_SLTH","Hypothetical SLT Health SCR"
"Project_Life_NAV","Life Net Asset Value Project"
"Project_Life_Premium_History","Life Historic Premium Project"
"Project_Life_Reinsurance_MKT_SCEN","Mkt Scenario of Life Reinsurance (Per Risk_mitigation_id -for Hyp Mkt SCR)"
"Project_Nonlife_Cat_Peril","Project Nonlife Catastrophe Peril Input"
"Project_Nonlife_Cat_SCR","Project Nonlife Catastrophe SCR Input"
"Project_Read_In_BE","Read in Scenario Value"
"Project_Read_In_BE_SLTH","SLTH Scenario Asset Value"
"Project_Read_In_VALUE","Cashflow Gap Analysis"
"Project_SCR_CCP_SUBRISK","Counterparty Cyclical Premium Project"
"Project_SCR_CCP_SUBRISK_NI","Counterparty Cyclical Premium Project NI"
"Project_SLTH_NAV","SLT Health Net Asset Value"
"Project_SubRiskAggregation","Market Solvency Project"
"Project_SubRiskAggregation_Life","Life Risk Aggregation Project"
"Project_SubRiskAggregation_MKT","Market SCR Aggregation"
"Project_SubRiskAggregation_MKT_RU","Market SCR Aggregation (Reference Undertaking)"
"Project_SubRiskAggregation_NL","Nonlife SCR Aggregation"
"Project_SubRiskAggregation_NSLTH","NSLT Health SCR Aggregation"
"Project_SubRiskAggregation_SLTH","SLT Health Risk Aggregation"
"Project_Val_Assets_MKT_MTM","Asset Mark-to-Market Valuation Project"
"Project_Val_Assets_MKT_SCN","Asset Scenario Valuation Project"
"Project_Val_Liab_Mkt_Scn","Nonlife Liability Valuation (Based on Market Scenarios) Project"
"Project_Val_Liab_Nl_Scn","Nonlife Liability Valuation (Based On Nonlife Scenarios) Project"
"Project_Val_Liab_nl_lap_scen","Nonlife Liability Valuation  (Based on Nonlife Lapse shock) Project"
"Project_Val_Liab_nslth_lap_scen","NSLTH Liability Valuation (Based on Nonlife Lapse shock) Project"
"Project_Valuation_Liabilities_NSLTH","NSLTH Liability Valuation"
"Project_Valuation_Liabilities_nl","Non-life Liability Valuation"
"Project_health_cat_gross_loss","Health Catastrophe Loss"
"Project_nl_cat","Non-life Catastrophe Risk Capital"
"Project_nl_cat_exposure","Non-Life Catastrophe Exposure"
"Project_nl_cat_loss","Non-Life Catastrophe Loss"
"Project_nl_cat_rm","Non-Life Catastrophe - Risk Mitigation"
"Project_nl_lapse","Nonlife Lapse Project"
"Project_nl_sigma","Nonlife Liability Sigma Measure Project"
"Project_nl_volume","Nonlife Liability Volume Measure Project"
"Project_nslth_lapse","NSLT Health Lapse Project"
"Project_nslth_sigma","NSLT Health Sigma Project"
"Project_nslth_volume","NSLT Health Volume Project"
"Project_reconciliation_reserves","Reconciliation Reserves After Adjustments"
"Project_restricted_own_fund","Restricted Own Fund"
"Project_source_excluded_reco_reserve","Source Excluded From Reconciliation Reserve"
"REPORT_SAMPLE","Sample Project for Report Tables"
"RISK_MARGIN_COC","Risk Margin Cost of Capital"
"RISK_MARGIN_SCR_RUNOFF","Risk margin SCR run off"
"Rollup_valuation","Property Interest Illiquidity Risk Capital Aggregation"
"SAMPLE_BSCR_AGGREGATION","Correlated BSCR Aggregation"
"SIMPLIFIED_LIFE_RM_BE_INPUT","Inputs for Simplified Life RM calculation(BE)"
"SIMPLIFIED_LIFE_RM_INPUT","Inputs for Simplified Life RM calculation"
"SLT_HEALTH","SLT Health Project"
"SPRD_RSK_CAPITAL_AGGR","Spread Risk Capital Aggregation"
"SP_SPRD_RSK_CAPITAL_AGGR","SP Spread Risk"
"SubSubRiskAggregation_NSLTH","NSLT Health P and R Risk"
"Sub_SubRiskAggregation","Nonlife SubSub Risk Aggregation "
"proj_intan_asset","Intangible Asset Risk Project"
"proj_op_risk","Operational Risk Project"
"proj_ri_exposure","Reinsurance Exposure Project"
"proj_ri_program","Reinsurance Program Project"
"proj_ri_share","Reinsurance Share Project"
"proj_risk_margin_cf_lf","Life Cash Flow Discounting Project"
"proj_risk_margin_cf_nl","Nonlife Cash Flow Discounting Project"
"project_full_int_model","Full Internal Model"
"project_nl_lapse_cf","Scenario Liability Value"
"project_nl_tp_cf","Cashflow Project for Claim Provision"
"project_part_int_model","Partial Internal Model"
;
run;

proc sort data=&syslast dupout=errors nodupkey;
by Project_id;

data _null_; set errors; 
   put "%str(ERR)OR: DUPLICATE PROJECT IDS IN PROVIDED DATA!!!";
run;

proc sql;
create table proj_dim1 as
   select a.*
      ,b.PROJECT_ID_DESC
   from &project_dim_dataset.(drop=project_id_desc) a
   left join unique_projects b
   on a.project_id=b.project_id
   order by a.project_sk;

data proj_dim2 errors;
if 0 then set &project_dim_dataset.;
set proj_dim1;
if project_id_desc = '' then output errors;
else output proj_dim2;
run;

data _null_; set errors; 
   put "%str(ERR)OR: SOME PROJECTS DO NOT HAVE DESCRIPTIONS!!!";
run;

proc sql;
create table errors as 
   select project_id_desc, count(distinct project_id) as test
   from proj_dim2
   group by 1
   having test>1;

data _null_; set errors; 
   put "%str(ERR)OR: Some project descriptions have more than one project!!!";
run;
/*
proc sql;
create table demo as select distinct project_id_desc, project_id from proj_dim2 
   where project_id_desc in (select project_id_desc from errors);
*/

%create_datalines_from_dataset(base_ds=proj_dim2, tgt_ds=work.PROJECT_DIM2 
   ,cards_file=&outfile.);

%inc &outfile/ lrecl=32000;
proc compare base=project_dim compare=project_dim2; run;

/* need to change the cards file to &TMPRRR.PROJECT_DIM !!! */
