%macro allocate_libs;
%global indata;
%rsk_get_swc_property(propertyName=risk.indata.root, outvar=indata);
%let root= %substr(&indata,1,%length(&indata)-7);
%let user=&sysuserid;
%let playpen=MARKET;
%let entity=Main;
%let mkt_pp=MARKET;
%let pp_root=&root.\data\userdata\&user\entities\&entity\playpens\&playpen;
%let PP_root_mkt=&root.\data\userdata\&user\entities\&entity\playpens\&mkt_pp;
libname staging "&root.\indata\staging";
libname mapping "&root.\indata\mapping";
libname static  "&root.\indata\static";
libname rd_stage "&pp_root\datamart\staging";
libname rd_mart "&pp_root\datamart";
libname rd_map ( "&pp_root\cfg\mapping"
               "&pp_root\datamart\cfg\mapping" );
libname rd_pos ("&pp_root\datamart\enriched_data\instdata");
libname rd_conf ( "&pp_root\cfg\static"
                  "&pp_root\datamart\cfg\static"
                  "&pp_root\cfg\static\corr_mats"
                  "&pp_root\datamart\cfg\static\corr_mats" );

libname rd_rpt "&root.\data\userdata\&user\reportmart";
libname mkt1 "&pp_root_mkt\results\asset_valuation_mtm";
libname mkt2 "&pp_root_mkt\results\asset_valuation_scen";

%mend;