/*
Generic macro to create a cards file from a SAS dataset.  

PARAMETERS:
   BASE_DS -> Should be two level - eg work.blah 
   TGT_DS  -> Table to be created by the cards file. Optional - if omitted, will be same as BASE_DS.
   CARDS_FILE -> Location to write the cards file (.sas) 

stuff to add
use of datalines4 to deal with embedded semicolons
labelling the dataset
explicity setting a unix LF 
(encoding=ASCIIANY) needs to go into DDL
*/


%macro cnvrt_ds2cards(base_ds=, tgt_ds= , cards_file=
                     , low_date=0, high_date='31DEC9999:23:59:59'dt );

%if %index(&base_ds,.)=0 %then %let base_ds=WORK.&base_ds;
%if (&tgt_ds = ) %then %let tgt_ds=&base_ds;
%if %index(&tgt_ds,.)=0 %then %let tgt_ds=WORK.&base_ds;

proc sql noprint;
create table datalines1 as 
   select name,type,length,varnum,format from dictionary.columns
   where libname="%upcase(%scan(&base_ds,1))" and memname="%upcase(%scan(&base_ds,2))";
select case when type='char' then cats('quote(strip(',name,'))') 
/*         when substr(name,length(strip(name))-1,2)='TM' then cats('strip(put(',name,',16.15))')*/
         else cats('strip(',name,')') end
   into: datalines separated by ',' 
   from datalines1 where name not in ('PROCESSED_DTTM','VALID_FROM_DTTM','VALID_TO_DTTM');

data _null_;
   set datalines1 end=lastobs;
   retain attrib_statement input_statement;
   length attrib_statement $32767 input_statement $32767;
/* build attrib statement */
   if type='char' then type2='$';
   if strip(format) ne '' then format2=cats('format=',format);
   str1=catx(' ',(put(name,$33.)||'length='),put(cats(type2,length),$7.),format2);
   attrib_statement=cat(trim(attrib_statement),'"   ',str1,'" / ');

/* Build input statement */
   if type='char' then type3=':$char.';
   str2=put(name,$33.)||type3;
   if name not in ('PROCESSED_DTTM','VALID_FROM_DTTM','VALID_TO_DTTM') 
      then input_statement=cat(trim(input_statement),'"   ',str2,'" / ');

   if lastobs then do;
      call symputx('ATTRIB',attrib_statement,'L');
      call symputx('INPUT ', input_statement,'L');
   end;
run;
data _null_;
   file &cards_file. lrecl=32767 termstr=nl;
   if _n_=1 then do;
      put '/********************************************************************************';
      put " Datalines for %upcase(%scan(&base_ds,2)) dataset ";
      put " Generated on %sysfunc(datetime(),datetime19.) by %nrstr(%%)cnvrt_ds2cards()";
      put '*********************************************************************************/';
      put "data &tgt_ds(encoding=ASCIIANY) ;";
      put "attrib ";
      put &attrib ";";
      %if %index(%quote(&attrib.),PROCESSED_DTTM) %then %do;
         put 'retain PROCESSED_DTTM %sysfunc(datetime());';
      %end;
      %if %index(%quote(&attrib.),VALID_FROM_DTTM) %then %do;
         put "retain VALID_FROM_DTTM &low_date;";
      %end;
      %if %index(%quote(&attrib.),VALID_TO_DTTM) %then %do;
         put "retain VALID_TO_DTTM &high_date;";
      %end;
      if nobs=0 then put 'stop;';
      else do;
         put "infile cards dsd delimiter=',';";
         put "input ";
         put &input ";";
         put "datalines4;";
      end;
   end;
   set &base_ds end=lastobs nobs=nobs;
   length dataline $32767;
   dataline=catx(',',&datalines);
   put dataline;
   if lastobs then do;
      put ';;;;';
      put 'run;';
   end;
run;

/*%put &attrib;*/
/*%put &input;*/
%mend;

