libname mapping "D:\TEMP\_TD11024\Prc2\map";
libname static "D:\TEMP\_TD11024\Prc2\conf";
libname staging "D:\SAS\config\Lev1\AppData\SASRiskManagementForInsurance\2.1\indata\staging";
libname mart "D:\SAS\config\Lev1\AppData\SASRiskManagementForInsurance\2.1\indata";

options nomprint nosource;

%macro cnvrt_lib2cards(lib=,outloc= /* without trailing slash */);
/* FIRST create the cards files */
%local x ds memlist;
proc sql noprint;* inobs=10;
select distinct lowcase(memname) into: memlist separated by ' ' from dictionary.tables 
   where libname="%upcase(&lib)";
%do x=1 %to %sysfunc(countw(&memlist));
   %let ds=%scan(&memlist,&x);
   %cnvrt_ds2cards(base_ds=&lib..&ds,tgt_ds=WORK.&ds,cards_file="&outloc.\&ds..sas")
%end;
/* SECOND execute the cards files (should go to WORK library) and compare with originals! */
/*%do x=1 %to %sysfunc(countw(&memlist));*/
/*   %let ds=%scan(&memlist,&x);*/
/*   %inc "&outloc.\&ds..sas" /lrecl=32000;*/
/*   proc compare noprint base=&lib..&ds(drop=valid_:) compare=WORK.&ds(drop=valid_:); run;*/
/*   %if %eval(&sysinfo ge 8) %then %do;*/
/*      proc compare base=&lib..&ds(drop=valid_:) compare=WORK.&ds (drop=valid_:) ; run;*/
/*   %end;*/
/*%end;*/

%mend;
%cnvrt_lib2cards(lib=static,outloc=D:\Program Files\SAS\SASFoundation\9.2\rmicomnsvr\sasmisc\solution_data_mart\sampledata\static);

%cnvrt_lib2cards(lib=mapping,outloc=D:\Program Files\SAS\SASFoundation\9.2\rmicomnsvr\sasmisc\solution_data_mart\sampledata\mapping);

%cnvrt_lib2cards(lib=staging,outloc=C:\Users\sas\Documents\phoenix_rmi21m1_f90011\rmicomnsvr\sasmisc\solution_data_mart\sampledata\staging);

%cnvrt_lib2cards(lib=mart,outloc=C:\Users\sas\Documents\phoenix_rmi21m1_f90011\rmicomnsvr\sasmisc\solution_data_mart\sampledata\mart);
